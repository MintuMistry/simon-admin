<?php 


namespace App\Controllers;

/*-----Load models-----*/
use App\Models\Login;
use App\Models\Admin_model;
/* use App\Models\Question;
use App\Models\Sponsor; */
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\HTTP\Files\UploadedFile;

class Admin extends BaseController
{

	public function __construct()
    {
        $this->session = session();
		//$this->ciqrcode->generate();
    }

	private function checkLogin()
	{
		if(!$this->session->get('admin_id')){

			// return redirect()->to();
			header("location: ".base_url('admin')."login");
			
		}
		else{
			
			return;
		}
	}

	private function loadView($viewname,$data = array())
	{
		echo view("admin/admin_header");
		echo view("admin/$viewname", $data);
		//echo view("admin/admin_footer");
	}
	
	private function loadView1($viewname,$data = array())
	{
		
		echo view("admin/$viewname", $data);
		
	}
	
	
	private function sendMail($to,$subject,$message)
	{
		mail($to,$subject,$message,array("from" => "no-reply@caviar.yesitlabs.xyz"));
	}
	
	public function index()
	{
		$this->checkLogin();
		//$users = new Users();
		$data =[];
		//$data['game_list'] = $users->crud_read_game();
		$this->loadView("admin/dashboard",$data);	
	}

	public function login()
	{
		echo view("admin/login");
	}

	public function login_check()
	{
		$login = new login();
		if(!empty($this->request->getPost('adminid')))
		{
			$adminid = $this->request->getPost('adminid');
			$users = new Users();
			$users->password_update(
					array("password" => $this->request->getPost("password")), $adminid);

			$this->session->set('admin_id', $adminid);
			$admin_data = $login->admin_data($adminid);
			$this->session->set('admin_data',$admin_data);
			return redirect()->to(base_url('admin/dashboard'));
			
		
		}else{
			
			$admin_id = $login->admin_verify(
				$this->request->getPost('email'),
				$this->request->getPost('password')
			);
			if ($admin_id >0){
				$this->session->set('admin_id', $admin_id);
				$this->session->admin_data = $login->admin_data($admin_id);
				return redirect()->to(base_url('admin/dashboard'));
			}
			else{
				$data['msg'] = "Incorrect Email or Password";
				$this->loadView1("admin/login", $data);
				
			}
		}
	}
	
	public function dashboard()
	{
		//$this->checkLogin();
		//$users = new Users();
		$data =[];
		$this->loadView("dashboard", $data);
	}
	

	public function logout()
	{
		$this->session->remove('admin_id');
		$this->session->destroy();
		return redirect()->to(base_url('admin/login') );
	}
	
	/* public function profile()
	{
		$this->checkLogin();
		$data =array();
		//$Admin_model = new Admin_model();
		//$data['org_list'] = $Admin_model->crud_read_org();
		$this->loadView("profile", $data);
	} */
	
	/* -----menu functions  */
	
	public function organisation_management()
	{
		$this->checkLogin();
		$data =array();
		$Admin_model = new Admin_model();
		$data['org_list'] = $Admin_model->crud_read_org();
		$this->loadView("organisation_management", $data);
	}
	
	public function update_org($orgId)
	{									
		$this->checkLogin();
		$data =array();
		$Admin_model = new Admin_model();
		$update_query = $Admin_model->crud_update_org(array(
					'orgName'=> $this->request->getPost('orgName'),
					'orgServiceCategory'=> $this->request->getPost('service_category'),
					'orgAddress'=> $this->request->getPost('address'),
					'orgCity'=> $this->request->getPost('city'),
					'orgState'=> $this->request->getPost('state'),
					'orgCountry'=> $this->request->getPost('country')
					),$orgId);
		$this->session->setFlashdata('success_msg', "Organisation details updated successfully");
		return redirect()->to(base_url('admin/organisation_management'));
	}
	
	public function installer_management()
	{
		$this->checkLogin();
		$data =array();
		$Admin_model = new Admin_model();
		$data['installer_list'] = $Admin_model->crud_read_installer();
		//$data['org_list'] = $Admin_model->crud_read_org();
		$this->loadView("installer_management", $data);
	}
	
	
	public function updateInstaller($id)
	{									
		$this->checkLogin();
		$data =array();
		$Admin_model = new Admin_model();
		
		$newName1 = "";
		$newName2 = "";
		
			if($img = $this->request->getFile('empProfilePic'))
			{
				
				if($_FILES['empProfilePic']['size'] > 10485760) { //10 MB (size is also in bytes)
					$this->session->setFlashdata('error_msg', "Upload file size is too big , It must be less than 10 MB.");
				} else {
					
					if ($img->isValid() && ! $img->hasMoved())
					{
					   $newName = $img->getRandomName();
					   $newName1 = date('ymd').'_'.$newName;
					   $photopath = $img->move('uploads', $newName1);
					   $update_query = $Admin_model->crud_update_installer(array(
						'empProfilePic'=> $newName1),$id);
					}
				}
			}
			
			
			$profile_img = $this->request->getFile('empProfilePic');
				if(!empty($profile_img)){
					if($profile_img->isValid()){
						$profile_imgpath = $profile_img->store();
							
						if($profile_imgpath != ""){
							
							$update_query = $Admin_model->crud_update_installer(array(
							'empProfilePic'=> $profile_imgpath),$id);
								
						}
					}
				}
				
			if($img1 = $this->request->getFile('Idproof'))
			{
				if($_FILES['Idproof']['size'] > 10485760) { //10 MB (size is also in bytes)
					$this->session->setFlashdata('error_msg', "Upload file size is too big , It must be less than 10 MB.");
				} else {
					
					if ($img1->isValid() && ! $img1->hasMoved())
					{
					   $newName = $img1->getRandomName();
					   $newName1 = date('ymd').'_'.$newName;
					   $photopath = $img1->move('uploads', $newName1);
					   $update_query = $Admin_model->crud_update_installer(array(
						'idProof'=> $newName1),$id);
					}
				}
			}
			
			/* $Idproof = $this->request->getFile('Idproof');
				if(!empty($Idproof)){
					if($Idproof->isValid()){
						$idproof_imgpath = $Idproof->store();
						
						if($idproof_imgpath != ""){
							$update_query = $Admin_model->crud_update_installer(array(
							'idProof'=> $idproof_imgpath),$id);
								
						}
					}
				} */
		$update_query = $Admin_model->crud_update_installer(array(
					'empName'=> $this->request->getPost('empName'),
					'streetAddress'=> $this->request->getPost('address'),
					'city'=> $this->request->getPost('city'),
					'state'=> $this->request->getPost('state'),
					'zip'=> $this->request->getPost('zip'),
					'country'=> $this->request->getPost('country'),
					'empMobNumber'=> $this->request->getPost('mobile'),
					'speciality'=> $this->request->getPost('speciality'),
					'dob'=> $this->request->getPost('dob'),
					'empEmail'=> $this->request->getPost('emailid'),
					'userLoginName'=> $this->request->getPost('login_name'),
					'password'=> $this->request->getPost('password')
					),$id);
					
					/* echo $Admin_model->getLastQuery();
					die(); */
		$this->session->setFlashdata('success_msg', "Installer details updated successfully");
		return redirect()->to(base_url('admin/installer_management'));
	}
	
	
	public function subscription_management()
	{
		$this->checkLogin();
		$data =array();
		$Admin_model = new Admin_model();
		$data['subscription_plan_list'] = $Admin_model->crud_read_subscription_plan();
		/* print_r($data);
		die(); */
		$this->loadView("subscription_plan", $data);
	}
	
	public function update_subscription_plan($planId)
	{
		$this->checkLogin();
		$data =array();
		$Admin_model = new Admin_model();
		$update_query = $Admin_model->crud_update_subsription_plan(array(
					'amountPerUser'=> $this->request->getPost('AmountPUser'),
					'plan_name'=> $this->request->getPost('PName')
					),$planId);
		$this->session->setFlashdata('success_msg', "Subscription Plan updated successfully");
		return redirect()->to(base_url('admin/subscription_management'));
	}
	
	
	public function transaction_management()
	{
		$this->checkLogin();
		$data =array();
		$Admin_model = new Admin_model();
		$data['transaction_list'] = $Admin_model->crud_read_org_transaction();
		$this->loadView("transaction_management", $data);
	}
	
	
	public function support_management()
	{
		$this->checkLogin();
		$data =array();
		$Admin_model = new Admin_model();
		$data['support_list'] = $Admin_model->crud_read_support();
		$this->loadView("support_management", $data);
	}
	
	public function update_support($supportId)
	{
		$this->checkLogin();
		$data =array();
		$Admin_model = new Admin_model();
		$update_query = $Admin_model->crud_update_support(array(
					'contact_person_name'=> $this->request->getPost('CPName'),
					'contact_phone_number'=> $this->request->getPost('CPPhone'),
					'contact_person_email'=> $this->request->getPost('CPEmail')
					),$supportId);
		$this->session->setFlashdata('success_msg', "Support details updated successfully");
		return redirect()->to(base_url('admin/support_management'));
	}
	
	
	public function invoice_management()
	{
		$this->checkLogin();
		$data =array();
		$login = new login();
		//$adminid = $this->session->get('admin_id');
		//$data['admin_data'] = $login->admin_data($adminid);
		$this->loadView("invoice_management", $data);
	}
}