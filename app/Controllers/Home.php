<?php 

namespace App\Controllers;
use App\Models\Login;
use App\Models\Users;
use App\Models\LoginOrg;
class Home extends BaseController
{
	public function __construct()
    {
        $this->session = session();
		//$this->ciqrcode->generate();
    }
	
	private function checkLogin()
	{
		if(!$this->session->get('admin_id')){
			//echo base_url('');
			//die();
			//return redirect()->to();
			//echo view("login");
			//header("location: ".base_url(''));
			return 1;
		}
		else{
			
			return;
		}
	}
	
	private function loadView($viewname,$data = array())
	{
		echo view("Templates/admin/header");
		echo view("header");
		echo view("$viewname", $data);
		//echo view("Templates/admin/footer");
	}
	
	private function loadView1($viewname,$data = array())
	{
		
		echo view("$viewname", $data);
		
	}
	
	private function loadWebView($viewname,$data = array())
	{
		echo view("web/header");
		echo view("web/$viewname", $data);
		echo view("web/footer");
		
	}
	
	private function sendMail($to,$subject,$message)
	{
		mail($to,$subject,$message,array("from" => "no-reply@caviar.yesitlabs.xyz"));
	}
	
	public function index()
	{
		$res = $this->checkLogin();
		if($res ==1)
		{
			echo view("login");
		}else{
			$users = new Users();
			$data =[];
			//$data['game_list'] = $users->crud_read_game();
			$this->loadView("dashboard",$data);	
		}
	}

	public function login()
	{
		echo view("login");
	}

	public function login_check()
	{
		$login = new LoginOrg();
		if(!empty($this->request->getPost('adminid')))
		{
			$adminid = $this->request->getPost('adminid');
			
			$login->password_update(
					array("password" => $this->request->getPost("password")), $adminid);

			$this->session->set('admin_id', $adminid);
			$admin_data = $login->admin_data($adminid);
			$this->session->set('admin_data',$admin_data);
			return redirect()->to(base_url(''));
			
		
		}else{
			
			$admin_id = $login->admin_verify(
				$this->request->getPost('email'),
				$this->request->getPost('password')
			);
			if ($admin_id >0){
				$this->session->set('admin_id', $admin_id);
				$this->session->admin_data = $login->admin_data($admin_id);
				return redirect()->to(base_url(''));
			}
			else{
				$data['msg'] = "Incorrect Email or Password";
				$this->loadView1("login", $data);
				
			}
		}
	}
	public function signup(){
		echo view("signup");
		//$data=[];
		//$this->loadView('signup',$data);
	}
	public function signupSubmit(){
		$signup = new LoginOrg();
		$data=array("orgName" => $this->request->getPost("fullName"),"orgEmail" => $this->request->getPost("email"),"orgPass" => $this->request->getPost("password"));
		$orgId = $signup->crud_create_org($data);
		print $signup->getLastQuery();
		if($orgId > 0)
		{
				$this->session->set('orgId', $orgId);
				$this->session->admin_data = $signup->admin_data($orgId);
				
				return redirect()->to(base_url(''));
		}
		return redirect()->to(base_url('login'));
	}
	
	public function forgotPassword()
	{
		//$this->checkLogin();
		$users = new Users();
		$data =[];
		$this->loadView1("forgot_password", $data);
	}
	
	public function recoverPassword()
	{
		//$this->checkLogin();
		$login = new login();
		$data =[];
		$admin_id = $login->email_check($this->request->getPost('email'));
		if ($admin_id >0){
			$email = $this->request->getPost('email');
			$msg = base_url('change_pass/'.$admin_id);
			$this->sendMail($email,"Recover Password Link",$msg);
			$data['msg'] = "Change Password Link Sent on your registered email id . Please change password by clicking on that link.";
			$this->loadView1("login", $data);
		}
		else{
			$data['msg'] = "Incorrect Email ID";
			$this->loadView1("forgot_password", $data);
			
		}
	}
	
	public function change_pass()
	{
		//$this->checkLogin();
		//$users = new Users();
		$data =[];
		$this->loadView1("confirm_password", $data);
	}
	
	public function password_update()
	{
		$this->checkLogin();
		$users = new Users();
		$user_id = $this->session->get('admin_id');
		$check_oldpass = $users->read_pass($this->request->getPost("oldpassword"),$user_id);
		
		if(!empty($check_oldpass)){
			$users->password_update(
					array("password" => $this->request->getPost("password")), $user_id);
			$this->session->setFlashdata('success_msg', "Password changed successfully");
			return redirect()->to(base_url('login') );
		}else{
			$this->session->setFlashdata('success_msg', "Old Password is not correct");
			return redirect()->to(base_url('change_pass') );
		}
		
	}
	
	public function logout()
	{
		$this->session->remove('admin_id');
		$this->session->destroy();
		return redirect()->to(base_url('') );
	}
	
	public function profile()
	{
		$this->checkLogin();
		$data =array();
		$this->loadView("profile", $data);
	}

	/* -----menu functions  */
	
	public function service_user_management()
	{
		$this->checkLogin();
		$data =array();
		$this->loadView("service_provider_user_management", $data);
	}
	
	public function project_manager_management()
	{
		$this->checkLogin();
		$data =array();
		$this->loadView("project_manager_management", $data);
	}
	
	
	public function assign_unassigned_task()
	{
		$this->checkLogin();
		$data =array();
		$this->loadView("assign_unassigned_task", $data);
	}
	
	
	public function job_history_management()
	{
		$this->checkLogin();
		$data =array();
		$this->loadView("job_history_management", $data);
	}
	
	
	public function customer_feedback_management()
	{
		$this->checkLogin();
		$data =array();
		$this->loadView("customer_feedback_management", $data);
	}
	
	
	
	/* -----menu functions  */
	//--------------------------------------------------------------------
		
		
}
