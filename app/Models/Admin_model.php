<?php  

namespace App\Models;


use CodeIgniter\Model;

class Admin_model extends Model
{
	protected $db;

    public function __construct()
    {
    
       	$this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('org');
   		$this->builder1 =  $this->db->table('installer');
   		$this->builder2 =  $this->db->table('tbl_transaction');
   		$this->builder3 =  $this->db->table('subsription_plan');
   		$this->builder4 =  $this->db->table('admin_support');
		
    }
	
	/* ------------get details functions -------------- */
	function crud_read_org()
	{	
		$this->builder->orderBy('orgId','DESC');
		return $this->builder->get()->getResultArray();
	}

	function crud_read_installer()
	{	
		$this->builder1->orderBy('empId','DESC');
		return $this->builder1->get()->getResultArray();
	}
	
	function crud_read_org_transaction()
	{	
		$this->builder2->orderBy('transaction_id','DESC');
		return $this->builder2->get()->getResultArray();
	}
	
	function crud_read_subscription_plan()
	{	
		$this->builder3->orderBy('plan_id','DESC');
		return $this->builder3->get()->getResultArray();
	}
	
	function crud_read_support()
	{	
		$this->builder4->orderBy('support_id','DESC');
		return $this->builder4->get()->getResultArray();
	}
	
	/* ------------get details functions  close-------------- */
	
	
	
	
	
	/* ------------create details functions -------------- */
	function crud_create_subsription_plan($data)
	{
		$this->builder3->insert($data);
		return $this->db->insertID();
	}
	
	/* ------------create details functions close-------------- */
	
	
	
	
	
	/* ------------update details functions -------------- */
	
	
	function crud_update_org($data, $orgid)
	{	
		$this->builder->where("orgId",$orgid);
		$this->builder->update($data);
	}
	
	function crud_update_installer($data, $installerId)
	{	
		$this->builder1->where("id",$installerId);
		$this->builder1->update($data);
	}
	
	
	function crud_update_subsription_plan($data, $planid)
	{	
		$this->builder3->where("plan_id",$planid);
		$this->builder3->update($data);
	}
	
	function crud_update_support($data, $supportId)
	{	
		$this->builder4->where("support_id",$supportId);
		$this->builder4->update($data);
	}
	
	/* ------------update details functions close-------------- */
	
}
?>