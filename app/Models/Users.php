<?php  

namespace App\Models;


use CodeIgniter\Model;

class Users extends Model
{
	protected $db;

    public function __construct()
    {
    
       	$this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('users');
   		$this->builder1 =  $this->db->table('teams');
   		$this->builder2 =  $this->db->table('team_attendee');
   		$this->builder3 =  $this->db->table('games');
   		$this->builder4 =  $this->db->table('admin_detail');
		$this->builder5 =  $this->db->table('question');
   		$this->builder6 =  $this->db->table('question_option');
   		$this->builder7 =  $this->db->table('user_answer');
   		$this->builder8 =  $this->db->table('user_result');
   		$this->builder9 =  $this->db->table('team_result');
   		$this->builder10 =  $this->db->table('website_management');
   		$this->builder11 =  $this->db->table('sponsor');
   		$this->builder12 =  $this->db->table('user_bid_answer');
   		$this->builder13 =  $this->db->table('contact_us');
		
    }
	
	function get_user_team($userid)
	{	
		$row = $this->builder2->where("userid",$userid)->get()->getRow();
		if (isset($row->team_id)) {
			return $row->team_id;
		}
		else{
			return 0;
		}
	}
	
	function create_user_bid_answer($data)
	{
		$this->builder12->insert($data);
		return $this->db->insertID();
	}
	
	function create_user_answer($data)
	{
		$this->builder7->insert($data);
		return $this->db->insertID();
	}
	
	function crud_create_website_link($data)
	{
		$this->builder10->insert($data);
		return $this->db->insertID();
	}
	
	function pagination_link($row,$rowperpage,$gameid)
	{
		$response = $this->db->query("SELECT * FROM website_management WHERE game_id =" .$gameid." ORDER BY indexid ASC limit $row,".$rowperpage)->getResultArray();
		return $response;
	}
	
	function set_question_status($data,$indexid)
	{
		//print_r($indexid);
		//die();
		$deactive_all = array("active_status" => 0);
		$this->builder10->update($deactive_all);
		
		$this->builder10->where("indexid",$indexid);
		$this->builder10->update($data);
	}
	
	function crud_delete_website_link($gameid)
	{	
		$this->builder10->where('game_id', $gameid);
		$this->builder10->delete();
	}
	
	function crud_delete_previous_game_record($gameid)
	{	
		$this->builder7->where('gameid', $gameid);
		$this->builder7->delete();
	
		$this->builder8->where('gameid', $gameid);
		$this->builder8->delete();
		
		$this->builder9->where('gameid', $gameid);
		$this->builder9->delete();
		
		$this->builder12->where('game_id', $gameid);
		$this->builder12->delete();
	}
	
	function crud_update_admin($data, $admin_id)
	{	
		$this->builder4->where("admin_id",$admin_id);
		$this->builder4->update($data);
	}
	
	function crud_create($data)
	{
		$this->builder->insert($data);
		return $this->db->insertID();
	}
	
	function crud_create_team($data)
	{
		$this->builder1->insert($data);
		return $this->db->insertID();
	}
	
	function crud_create_game($data)
	{
		$this->builder3->insert($data);
		return $this->db->insertID();
	}
	
	function crud_update_game($data, $gameid)
	{	
		$this->builder3->where("game_id",$gameid);
		$this->builder3->update($data);
	}
	
	function crud_delete_game($gameid)
	{	
		$this->builder3->where('game_id', $gameid);
		$this->builder3->delete();
		
		$this->builder8->where('gameid', $gameid);
		$this->builder8->delete();
		
		$this->builder7->where('gameid', $gameid);
		$this->builder7->delete();

		$this->builder9->where('gameid', $gameid);
		$this->builder9->delete();
		
		$this->builder10->where('game_id', $gameid);
		$this->builder10->delete();
		
	}
	
	function crud_read_game_by_name($game_name)
	{	
		$row = $this->builder3->where("game_name",$game_name)->get()->getRow();
		if (isset($row->game_id)) {
			return $row->game_id;
		}
		else{
			return 0;
		}
	}
	
	function crud_read_game_by_quizid($quizid)
	{	
		$row = $this->builder3->where("quizid",$quizid)->get()->getRow();
		if (isset($row->game_id)) {
			return $row->game_id;
		}
		else{
			return 0;
		}
	}
	
	
	function crud_read_user_game($userid ,$game_id='' )
	{	
		if($game_id !='')
		{
			$row = $this->builder7->where("userid",$userid)->where("gameid",$game_id)->get()->getRow();
			if (isset($row->answer_id)) {
				return $row->answer_id;
			}
			else{
				return 0;
			}
			
		}else{
			
			$row = $this->builder7->where("userid",$userid)->get()->getRow();
			if (isset($row->answer_id)) {
				return $row->answer_id;
			}
			else{
				return 0;
			}
		}
		
	}
	
	
	
	function crud_read_sponsor_by_name($sponsor_name)
	{	
		$row = $this->builder11->where("sponsor_name",$sponsor_name)->get()->getRow();
		if (isset($row->sponsor_id)) {
			return $row->sponsor_id;
		}
		else{
			return 0;
		}
	}
	
	
	
	function crud_read_user_by_emailmobile($email , $mobile)
	{	
	
		if(!empty($email))
		{
			$row = $this->builder->where("email", $email)->get()->getRow();
		}else{
			$row = $this->builder->where("phone_number", $mobile)->get()->getRow();
		}
		
		if (isset($row->user_id)) {
			
			return $row->user_id;
		}
		else{
			return 0;
		}
			
	}
	
	
	
	function crud_read_ques_by_name($ques)
	{	
		$row = $this->builder5->where("question_text", $ques)->get()->getRow();
		if (isset($row->question_id)) {
			
			return $row->question_id;
		}
		else{
			return 0;
		}
			
	}
	
	function crud_read_team_by_name($teamname)
	{	
		$row = $this->builder1->where("team_name", $teamname)->get()->getRow();
		if (isset($row->team_id)) {
			
			return $row->team_id;
		}
		else{
			return 0;
		}
			
	}
	
	function crud_read_team_member($team_id)
	{	
			return $this->builder2->where("team_id", $team_id)->get()->getResultArray();	
	}
	
	
	function crud_read_contact()
	{	
		$this->builder13->orderBy('contact_us_id','DESC');
		return $this->builder13->get()->getResultArray();
	}
	
	
	function crud_read_game($game_id = '')
	{	
		if($game_id > 0){
			$this->builder3->where("game_id",$game_id);
			$this->builder3->orderBy('game_id','DESC');
			return $this->builder3->get()->getResultArray();
			
		}
		else{
			$this->builder3->orderBy('game_id','DESC');
			return $this->builder3->get()->getResultArray();
		}
	}
	
	function crud_read_game_type($type)
	{	
		$this->builder3->where("game_type",$type);
		$this->builder3->orderBy('game_id','DESC');
		return $this->builder3->get()->getResultArray();
	}
	
	function crud_read($userid = '')
	{	
		if($userid > 0){
			$this->builder->where("user_id",$userid);
			$this->builder->orderBy('user_id','DESC');
			return $this->builder->get()->getResultArray();
			
		}
		else{
			$this->builder->orderBy('user_id','DESC');
			return $this->builder->get()->getResultArray();
		}
	}
	
	function crud_read_team($teamid = '')
	{	
		if($teamid > 0){
			$this->builder1->where("team_id",$teamid);
			$this->builder1->orderBy('team_id','DESC');
			return $this->builder1->get()->getResultArray();
			
		}
		else{
			$this->builder1->orderBy('team_id','DESC');
			return $this->builder1->get()->getResultArray();
		}
	}
	
	function crud_read_assigned_team($userid)
	{	
		$response = $this->db->query("SELECT team_attendee.*,teams.team_name FROM team_attendee LEFT JOIN teams ON team_attendee.team_id = teams.team_id  WHERE team_attendee.userid =" .$userid)->getResultArray();
		return $response;	
		
		//$this->builder2->where("userid",$userid);
		//return $this->builder2->get()->getResultArray();
	}
	
	function crud_read_team_result($gameid = '')
	{	
	//echo $gameid;
	//die();
		if($gameid > 0){
			$response = $this->db->query("SELECT team_result.*,teams.team_name FROM team_result LEFT JOIN teams ON team_result.teamid = teams.team_id  WHERE team_result.gameid =" .$gameid." ORDER BY gameid")->getResultArray();
			return $response;
		
			/* $this->builder9->where("gameid",$gameid);
			$this->builder9->orderBy('gameid','DESC');
			return $this->builder9->get()->getResultArray(); */
			
		}
		else{
			$response = $this->db->query("SELECT team_result.*,teams.team_name FROM team_result LEFT JOIN teams ON team_result.teamid = teams.team_id ORDER BY gameid")->getResultArray();
			return $response;
			
			//$this->builder9->orderBy('gameid','DESC');
			//return $this->builder9->get()->getResultArray();
		}
	}
	
	function crud_read_result($gameid = '' ,$teamid = '')
	{	
		if($teamid > 0){
			$response = $this->db->query("SELECT user_result.*,users.full_name FROM user_result LEFT JOIN users ON user_result.userid = users.user_id  WHERE user_result.gameid =" .$gameid." AND user_result.teamid =" .$teamid)->getResultArray();
			return $response;
			
			/* $this->builder8->where("gameid",$gameid);
			$this->builder8->where("teamid",$teamid);
			return $this->builder8->get()->getResultArray(); */
			
		}
		else{
			
			$response = $this->db->query("SELECT user_result.*,users.full_name FROM user_result LEFT JOIN users ON user_result.userid = users.user_id  WHERE user_result.gameid =" .$gameid." ORDER BY user_result.user_rank")->getResultArray();
			return $response;
			
			//$this->builder8->where("gameid",$gameid);
			//return $this->builder8->get()->getResultArray();
		}
	}
	
	function crud_user_answer($gameid = '')
	{
		$response = $this->db->query("SELECT user_answer.*,users.full_name ,question_option.option_text FROM user_answer LEFT JOIN users ON user_answer.userid = users.user_id LEFT JOIN question_option ON user_answer.answer_given = question_option.option_id WHERE gameid =" .$gameid)->getResultArray();
		return $response;
	}
	
	
	function crud_read_user_answer($userid = '',$gameid = '')
	{	
		if($userid > 0){
			
			$response = $this->db->query("SELECT user_answer.*,users.full_name ,teams.team_name ,question_option.option_text FROM user_answer LEFT JOIN users ON user_answer.userid = users.user_id LEFT JOIN teams ON user_answer.teamid = teams.team_id LEFT JOIN question_option ON user_answer.answer_given = question_option.option_id WHERE gameid =" .$gameid." AND user_answer.userid =" .$userid)->getResultArray();
			return $response;	
		}
		else{
			
			$response = $this->db->query("SELECT user_answer.*,users.full_name ,teams.team_name ,question_option.option_text FROM user_answer LEFT JOIN users ON user_answer.userid = users.user_id LEFT JOIN teams ON user_answer.teamid = teams.team_id LEFT JOIN question_option ON user_answer.answer_given = question_option.option_id WHERE gameid =" .$gameid)->getResultArray();
			return $response;
		}
	}
	
	function crud_read_user_answer_by_question($quesid = '',$gameid = '')
	{	
		if($quesid > 0){
			
			$response = $this->db->query("SELECT user_answer.*,users.full_name ,teams.team_name ,question_option.option_text FROM user_answer LEFT JOIN users ON user_answer.userid = users.user_id LEFT JOIN teams ON user_answer.teamid = teams.team_id LEFT JOIN question_option ON user_answer.answer_given = question_option.option_id WHERE gameid =" .$gameid." AND user_answer.quesid =" .$quesid)->getResultArray();
			return $response;	
		}
		else{
			
			$response = $this->db->query("SELECT user_answer.*,users.full_name ,teams.team_name ,question_option.option_text FROM user_answer LEFT JOIN users ON user_answer.userid = users.user_id LEFT JOIN teams ON user_answer.teamid = teams.team_id LEFT JOIN question_option ON user_answer.answer_given = question_option.option_id WHERE gameid =" .$gameid)->getResultArray();
			return $response;
		}
	}
	
	function total_user_response($quesid,$gameid)
	{	
		$response = $this->db->query("SELECT answer_id,answer_given FROM user_answer WHERE answer_given !='' AND gameid =" .$gameid." AND quesid =" .$quesid)->getResultArray();
		return $response;	
	}
	function get_users_answer($gameid)
	{	
		$this->builder7->where("gameid",$gameid);
		return $this->builder7->get()->getResultArray();
	}
	
	function get_users_total($gameid,$userid)
	{	
		$this->builder7->where("userid",$userid);
		$this->builder7->where("gameid",$gameid);
		return $this->builder7->get()->getResultArray();
	}
	
	function check_previous_user_result($userid,$gameid)
	{	
		$row = $this->builder8->where("gameid",$gameid)->where("userid",$userid)->get()->getRow();
		if (isset($row->result_id)) {
			return $row->result_id;
		}
		else{
			return 0;
		}
	}
	
	function create_user_result($data)
	{
		$this->builder8->insert($data);
		return $this->db->insertID();
	}
	
	function get_team_total($gameid,$teamid)
	{	
		$this->builder8->where("teamid",$teamid);
		$this->builder8->where("gameid",$gameid);
		return $this->builder8->get()->getResultArray();
	}
	
	function get_user_bonus_points($gameid,$userid)
	{	
		$this->builder12->where("userid",$userid);
		$this->builder12->where("game_id",$gameid);
		return $this->builder12->get()->getResultArray();
	}
	
	function check_previous_team_result($teamid,$gameid)
	{	
		$row = $this->builder9->where("teamid",$teamid)->where("gameid",$gameid)->get()->getRow();
		if (isset($row->teamresult_id)) {
			return $row->teamresult_id;
		}
		else{
			return 0;
		}
	}
	
	function create_team_result($data)
	{
		/* echo "<pre>";
		print_r($data);
		die(); */
		$this->builder9->insert($data);
		return $this->db->insertID();
	}
	
	function read_team_result($gameid)
	{	
		$this->builder9->where("gameid",$gameid);
		return $this->builder9->get()->getResultArray();
	}
	
	function read_individual_result($gameid)
	{	
		$this->builder8->where("gameid",$gameid);
		return $this->builder8->get()->getResultArray();
	}
	
	function read_team_result_rank($gameid)
	{	
		$this->builder9->where("gameid",$gameid);
		$this->builder9->orderBy('tpoints','DESC');
		return $this->builder9->get()->getResultArray();
	}
	
	function read_user_result_rank($gameid)
	{	
		$this->builder8->where("gameid",$gameid);
		$this->builder8->orderBy('total_points','DESC');
		return $this->builder8->get()->getResultArray();
	}
	
	function check_previous_team_rank($tpoints,$gameid)
	{	
		$this->builder9->where("tpoints",$tpoints)->where("gameid",$gameid)->get()->getRow();
		return $this->builder9->get()->getResultArray();
	}
	
	function update_user_rank($data,$result_id)
	{	
		$this->builder8->where("result_id",$result_id);
		$this->builder8->update($data);
	}
	
	
	function update_team_rank($data,$teamresult_id)
	{	
		$this->builder9->where("teamresult_id",$teamresult_id);
		$this->builder9->update($data);
	}
	
	
	function option_response($optionid,$gameid)
	{	
		$response = $this->db->query("SELECT answer_id,answer_given FROM user_answer WHERE gameid =" .$gameid." AND answer_given =" .$optionid)->getResultArray();
		return $response;	
	}
	
	
	function crud_edit_assign_team($data, $user_id)
	{	
		$this->builder2->where("userid",$user_id);
		$this->builder2->update($data);
	}
	
	function crud_delete_attendee($team_id,$user_id)
	{	
		$this->builder2->where('team_id', $team_id);
		$this->builder2->where("userid",$user_id);
		$this->builder2->delete();
	}
	
	function crud_delete_team($team_id)
	{	
		$this->builder1->where('team_id', $team_id);
		$this->builder1->delete();
		
		$this->builder2->where('team_id', $team_id);
		$this->builder2->delete();
		
		
	}
	
	function crud_update_team($data, $team_id)
	{	
		$this->builder1->where("team_id",$team_id);
		$this->builder1->update($data);
	}
	
	function crud_assign_team($data)
	{	
		$this->builder2->insert($data);
		return $this->db->insertID();
	}
	

	function crud_update($data, $user_id)
	{	
		$this->builder->where("user_id",$user_id);
		$this->builder->update($data);
	}
	
	function read_pass($oldpass, $user_id)
	{	
		//$response = $this->db->query("SELECT * FROM admin_detail WHERE admin_id =" .$user_id. " AND password =" .$oldpass)->getResultArray();
		//return $response;	
		
		$this->builder7->where('admin_id', $user_id)->where("password", $oldpass);
		return $this->builder7->get()->getResultArray();
	}
	
	function password_update($data, $user_id)
	{	
	
		$this->builder4->where("admin_id",$user_id);
		$this->builder4->update($data);
		//return TRUE;
	}

	function crud_delete($user_id)
	{	
		$this->builder->where('user_id', $user_id);
		$this->builder->delete();
		
		$this->builder2->where('userid', $user_id);
		$this->builder2->delete();

		$this->builder7->where('userid', $user_id);
		$this->builder7->delete();
		
		$this->builder8->where('userid', $user_id);
		$this->builder8->delete();
		
	}

	function read_by_mail_or_phone($mail,$phone)
	{
		if(!empty($mail))
			return $this->builder->where("email", $mail)
					->get()->getRowArray();
		else
			return $this->builder->where("phone_number", $phone)->get()->getRowArray();

		//return $this->db->query("SELECT * FROM users WHERE user_email='$mail' OR mobile='$phone'")->getResultArray();
	}
	
	function read_by_userid($user_id)
	{
		
		if(!empty($user_id))
		{
			return $this->builder->where("user_id", $user_id)->get()->getResultArray();
		}
	}

	function credentials_check($mail, $phone, $password)
	{
		if(!empty($mail))
			return $this->builder->where("email", $mail)->where("password", $password)
					->get()->getResultArray();
		else
			return $this->builder->where("phone_number", $phone)->where("password", $password)->get()->getResultArray();
	}

	function read_game_by_location($location)
	{
		return $this->builder3->where("game_location", $location)->get()->getResultArray();
		
	}
	
	
	function today_game_by_location($location)
	{
		$today = date('m/d/Y');
		return $this->builder3->like("game_location", $location)->where("game_date", $today)->get()->getResultArray();
		
	}
	
	
	function read_game_by_quiz_id($quiz_id)
	{
		//return $this->db->query("SELECT * FROM games WHERE quizid like binary ='$quiz_id'")->getResultArray();
		
		return $this->builder3->where("quizid like binary ", $quiz_id, NULL, FALSE)->get()->getRowArray();
		
	}
	
	function read_game_by_location_wager($location)
	{
		return $this->builder3->where("game_location", $location)->where("wager_ques", 1)->get()->getResultArray();
		
	}
	
	
}
?>