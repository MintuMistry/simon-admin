﻿<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="Bootlab">
	<title> Simon Says </title>
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,500;1,600;1,700;1,800;1,900&family=Nunito+Sans:ital,wght@0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="css/upload-img.css">
	<link rel="stylesheet" href="css/classic.css">
</head>

<body>
	<main class="main conexbg">
		<div class="container">
			<div class="row align-items-center justify-content-center">
				<div class="col-sm-10 col-md-8 col-lg-7 mt-80">
					<div class="card">
						<div class="upload-profile-box">
							<div class="upload-box posi-relative">
							     <div class="circle circl-new">
							       <img class="profile-pic profile-pic-new" src="img/dummy-prof.jpg" alt="profile">
							     </div>
							     <div class="p-image p-image-new">
							       <i class="fas fa-pencil-alt upload-button"></i>
							        <input class="file-upload" type="file" accept="image/*">
							     </div>
							</div>
						</div>
						<div class="card-header mt-4">
							<h5 class="card-title mb-0">Personal info</h5>
						</div>
						<div class="card-body pt-0">
							<form id="formfont">
								<div class="form-row">
									<div class="form-group col-md-6">
										<label for="">Full Name</label>
										<input type="text" class="form-control" id="" placeholder="">
									</div>
									<div class="form-group col-md-6">
										<label for="">Phone Number</label>
										<input type="text" class="form-control" id="" placeholder="">
									</div>
								</div>
								<div class="form-row">
									<div class="form-group col-md-6">
										<label for="">Email</label>
										<input type="email" class="form-control" id="" placeholder="">
									</div>
									
								</div>

								<h5 class="card-title pl-lg-0 mb-0">Organization Information </h5>
								<div class="form-row">
									<div class="form-group col-md-6">
										<label for="inputAddress">Organization  Name</label>
										<input type="text" class="form-control" id="" placeholder="">
									</div>
									<div class="form-group col-md-6">
										<label for="inputAddress2">Organization  ID</label>
										<input type="text" class="form-control" id="" placeholder="">
									</div>
								</div>
								<div class="form-row">
									<div class="form-group col-md-6">
										<label for="inputCity">Organization Service Category</label>
										<input type="text" class="form-control" id="" placeholder="">
									</div>
									<div class="form-group col-md-6">
										<label for="inputCity">Organization  Mail ID</label>
										<input type="text" class="form-control" id="" placeholder="">
									</div>
									
								</div>
								<div class="form-row">
									<div class="form-group col-md-6">
										<label for="inputCity">Street Address</label>
										<input type="text" class="form-control" id="" placeholder="">
									</div>
									<div class="form-group col-md-6">
										<label for="inputCity">City</label>
										<input type="text" class="form-control" id="" placeholder="">
									</div>
									
								</div>
								<div class="form-row">
									<div class="form-group col-md-6">
										<label for="inputCity">State</label>
										<input type="text" class="form-control" id="" placeholder="">
									</div>
									<div class="form-group col-md-6">
										<label for="inputCity">Zip</label>
										<input type="text" class="form-control" id="" placeholder="">
									</div>
									
								</div>
								<div class="form-row">
									<div class="form-group col-md-6">
										<label for="inputZip">Country</label>
										<input type="text" class="form-control" id="" placeholder="">
									</div>
									<div class="form-group col-md-6 text-right">
										<a class="btn profile-submit-btn mt-4" href="subscription_plan.html"> Submit </a>
									</div>
								</div>										
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>

	<script src="<?php echo base_url(); ?>/public/assets/js/app.js"></script>
	<script  src="<?php echo base_url(); ?>/public/assets/js/upload-img.js"></script>

</body>

</html>