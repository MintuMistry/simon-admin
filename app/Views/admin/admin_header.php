<?php
$session = \Config\Services::session($config);

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="Bootlab">
	<title> Simon Says </title>
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,500;1,600;1,700;1,800;1,900&family=Nunito+Sans:ital,wght@0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/public/assets/css/classic.css">
</head>

<body>
	<div class="wrapper">
		<nav id="sidebar" class="sidebar">
			<div class="sidebar-content ">
			<a class="sidebar-brand" href="<?php echo base_url(); ?>">
          		<span class="align-middle"><img src="<?php echo base_url(); ?>/public/assets/images/logo.png" alt="logo" class="img-responsive"></span>
        	</a>

				<ul class="sidebar-nav">
					<li class="sidebar-item active">
						<a href="<?php echo base_url('admin/dashboard'); ?>" class="sidebar-link">
			             <span class="align-middle">Dashboard</span>
			            </a>
					</li>
					<li class="sidebar-item">
						<a href="<?php echo base_url('admin/organisation_management'); ?>" class="sidebar-link">
              				<span class="align-middle">Organisation Management  </span>
            			</a>
					</li>
					<li class="sidebar-item">
						<a href="<?php echo base_url('admin/installer_management'); ?>" class="sidebar-link">
              				<span class="align-middle">Installer Management </span>
            			</a>
					</li>
					<li class="sidebar-item">
						<a href="<?php echo base_url('admin/subscription_management'); ?>" class="sidebar-link">
              				<span class="align-middle"> Subscription Management </span>
            			</a>
					</li>
					<li class="sidebar-item">
						<a href="<?php echo base_url('admin/transaction_management'); ?>" class="sidebar-link">
              				<span class="align-middle">Transaction Management </span>
            			</a>
					</li>
					<li class="sidebar-item">
						<a href="<?php echo base_url('admin/invoice_management'); ?>" class="sidebar-link">
              				<span class="align-middle"> Invoice Management </span>
            			</a>
					</li>
					
					<li class="sidebar-item">
						<a href="<?php echo base_url('admin/support_management'); ?>" class="sidebar-link">
              				<span class="align-middle"> Support Management </span>
            			</a>
					</li>
				</ul>
			</div>
		</nav>

		<div class="main">
			<nav class="navbar navbar-expand navbar-light bg-white">
				<a class="sidebar-toggle d-flex mr-2">
		          <i class="hamburger align-self-center"></i>
		        </a>
				<div class="navbar-collapse collapse">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle d-none d-sm-inline-block" href="#" data-toggle="dropdown">
				                <img src="<?php echo base_url(); ?>/public/assets/images/avatars/avatar.jpg" class="avatar img-fluid rounded-circle mr-1" alt="Chris Wood"> <span class="text-dark">Admin </span>
				              </a>
							<div class="dropdown-menu dropdown-menu-right">
								<!--<a class="dropdown-item" href="<?php echo base_url(); ?>/admin/profile"> Profile</a>
								<div class="dropdown-divider"></div>-->
								<a class="dropdown-item" href="<?php echo base_url(); ?>/admin/logout">Log out</a>
							</div>
						</li>
					</ul>
				</div>
			</nav>