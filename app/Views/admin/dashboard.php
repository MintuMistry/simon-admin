﻿
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
  
</button>
<?php $this->session = \Config\Services::session()?>
<?php if($this->session->success_msg){ ?>
	<div class="alert alert-success alert-dismissible fade in">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
	<strong>Success!  </strong><?php echo $this->session->success_msg; ?></div>
<?php } if($this->session->error_msg){?>
<div class="alert alert-success alert-dismissible fade in">
<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
<strong>Fail!   </strong><?php echo $this->session->error_msg; ?></div>
<?php } ?>			
			<main class="content dashboard-box">
				<div class="container-fluid p-0">
					<div class="row">
					<div class="col-sm-3">
							<div class="card dasbox">
								<div class="card-header">
									<h5 class="dash-txt"> Number of Organisations </h5>
								</div>
								<h2 class="dash-number"> 130 </h2>
								<div class="progress progress-sm shadow-sm mb-1">
										<div class="progress-bar bg-warning" role="progressbar" style="width: 57%"></div>
								</div>
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="card dasbox">
								<div class="card-header">
									<h5 class="dash-txt"> Number of installers </h5>
								</div>
								<h2 class="dash-number"> 1721 </h2>
								<div class="progress progress-sm shadow-sm mb-1">
										<div class="progress-bar bg-warning" role="progressbar" style="width: 57%"></div>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="card dasbox">
								<div class="card-header">
									<h5 class="dash-txt"> Number of project manager </h5>
								</div>
								<h2 class="dash-number"> 123 </h2>
								<div class="progress progress-sm shadow-sm mb-1">
										<div class="progress-bar bg-red" role="progressbar" style="width: 82%"></div>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="card dasbox">
								<div class="card-header">
									<h5 class="dash-txt"> Number of the job completed </h5>
								</div>
								<h2 class="dash-number"> 120 </h2>
								<div class="progress progress-sm shadow-sm mb-1">
										<div class="progress-bar bg-green" role="progressbar" style="width: 57%"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>

			<!-- <footer class="footer"></footer> -->
		</div>
	</div>
<!-- <script src="js\settings.js"></script> -->
	<script src="<?php echo base_url(); ?>/public/assets/js/app.js"></script>
	<script>
		$(function() {
			$("#datatables-dashboard-projects").DataTable({
				pageLength: 6,
				lengthChange: false,
				bFilter: false,
				autoWidth: false
			});
		});
	</script>
</body>

</html>