﻿
			<main class="content dashboard-box">

				<div class="container-fluid p-0">
					<div class="row">
									<div class="col-12 col-lg-12 mt-minus">
									<div class="table-responsive  border">
									<table class="table mb-0">
										<thead>
											<tr>
												<th scope="col">Emp ID </th>
												<th scope="col" style="">User Name</th>
												<th scope="col" style="">Mobile Number</th>
												<th scope="col" style="">Email ID</th>
												<th scope="col" class="text-center">Number of Jobs <br> Today </th>
												<th scope="col" class="text-center">Number of Jobs <br> Completed Today</th>
												<th scope="col">Action</th>
											</tr>
										</thead>
										<tbody>
										<?php 
										if(!empty($installer_list))
										{
											foreach($installer_list as $list){
										?>
										
			


		<!--------------- View Installer Modal ---------------->

			     			<div class="modal fade show" id="editinstaller1<?=$list['id']?>" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h3 class="modal-title"> View Organisation</h3>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						                      <span aria-hidden="true">×</span>
						                    </button>
										</div>
										<div class="modal-body">
													
														<div id="formbox">
														  <div class="form-row">
														    <div class="form-group col-md-6">
														      <label for=""> User Name </label>
														      <input type="text" class="form-control" name="empName" value="<?=$list['empName'] ?>" readonly>
														    </div>
														    	<div class="form-group col-md-6">
														      <label for="">Street Address</label>
														      <input type="text" class="form-control" name="address" value="<?=$list['streetAddress'] ?>" readonly>
														    </div>
														  </div>
														   <div class="form-row">
														   
														    <div class="form-group col-md-6">
														      <label for="">City</label>
														      <input type="text" class="form-control" name="city" value="<?=$list['city'] ?>" readonly>
														    </div>
															<div class="form-group col-md-6">
																	<label for="inputAddress">State</label>
																	<input type="text" class="form-control" name="state" value="<?=$list['state'] ?>" readonly>
																</div>
																
														  </div>
														   <div class="form-row">
																
																<div class="form-group col-md-6">
																	<label for="inputAddress2">Zip</label>
																	<input type="text" class="form-control" name="zip" value="<?=$list['zip'] ?>" readonly>
																</div>
																
																<div class="form-group col-md-6">
																	<label for="inputCity">Country</label>
																	<input type="text" class="form-control" name="country" value="<?=$list['country'] ?>" readonly>
																</div>
																
															</div>
															<div class="form-row">
																
																<div class="form-group col-md-6">
																	<label for="inputState">User Mobile Number </label>
																	<input type="text" class="form-control" name="mobile" value="<?=$list['empMobNumber'] ?>" readonly>
																</div>
																
																<div class="form-group col-md-6">
																	<label for="inputZip">Specialty</label>
																	<input type="text" class="form-control" name="speciality" value="<?=$list['speciality'] ?>" readonly>
																</div>
																
															</div>
															<div class="form-row">
																
																<div class="form-group col-md-6">
																	<label for="inputZip">Date of Birth</label>
																	<input type="date" data-date="" data-date-format="MMMM DD YYYY" value="<?=$list['dob'] ?>" class="form-control" name="dob" placeholder="" readonly>
																</div>
																
																<div class="form-group col-md-6">
																	<label for="inputZip"> User Email ID </label>
																	<input type="text" class="form-control" name="emailid" value="<?=$list['empEmail'] ?>" readonly>
																</div>
															</div>
															<div class="form-row">
																
																<div class="form-group col-md-6">
																	<label for="inputZip"> User Login Name </label>
																	<input type="text" class="form-control" name="login_name" value="<?=$list['userLoginName'] ?>" readonly>
																</div>
															</div>
															
														 
													</div>
												
										</div>
									</div>
								</div>
							</div>
				<!-------- View Installer End Modal -------->
				
				
									<!--------------- Edit org Modal ---------------->
			     			<div class="modal fade show" id="editinstaller<?=$list['id']?>" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
									<form action="<?php echo base_url();?>/admin/updateInstaller/<?php echo $list['id']; ?>" method="post" enctype="multipart/form-data" >
										<div class="modal-header">
										   <div class="upload-box">
											     <div class="circle">
												 <?php 
													if(!empty($list['empProfilePic']))
													{?>
														<img class="profile-pic" src="<?php echo base_url()."/public/uploads/".$list['empProfilePic']; ?>" alt="profile">
														
													<?php }else{?>
													<img class="profile-pic" src="<?php echo base_url(); ?>/public/assets/images/dummy-prof.jpg" alt="profile">
												<?php													
													}
												 ?>
											       
											     </div>
											     <div class="p-image pop-update">
											       <i class="fas fa-pencil-alt upload-button"></i>
											        <input class="file-upload" type="file" name="empProfilePic" accept="image/*"/>
											     </div>
											</div>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						                      <span aria-hidden="true">×</span>
						                    </button>
										</div>
										<div class="modal-body">
													
														<div id="formbox">
														  <div class="form-row">
														    <div class="form-group col-md-6">
														      <label for=""> User Name </label>
														      <input type="text" class="form-control" name="empName" value="<?=$list['empName'] ?>" >
														    </div>
														    <div class="form-group col-md-6">
														      <label for="">User ID Proof </label>
															   <input type="file" name="Idproof" accept="image/*"/>
														    </div>
														  </div>
														   <div class="form-row">
														   	<div class="form-group col-md-6">
														      <label for="">Street Address</label>
														      <input type="text" class="form-control" name="address" value="<?=$list['streetAddress'] ?>" >
														    </div>
														    <div class="form-group col-md-6">
														      <label for="">City</label>
														      <input type="text" class="form-control" name="city" value="<?=$list['city'] ?>" >
														    </div>
														  </div>
														   <div class="form-row">
																<div class="form-group col-md-6">
																	<label for="">State</label>
																	<input type="text" class="form-control" name="state" value="<?=$list['state'] ?>" >
																</div>
																<div class="form-group col-md-6">
																	<label for="">Zip</label>
																	<input type="text" class="form-control" name="zip" value="<?=$list['zip'] ?>" >
																</div>
															</div>
															<div class="form-row">
																<div class="form-group col-md-6">
																	<label for="">Country</label>
																	<input type="text" class="form-control" name="country" value="<?=$list['country'] ?>" >
																</div>
																<div class="form-group col-md-6">
																	<label for="">User Mobile Number </label>
																	<input type="text" class="form-control" name="mobile" value="<?=$list['empMobNumber'] ?>" >
																</div>
															</div>
															<div class="form-row">
																<div class="form-group col-md-6">
																	<label for="">Specialty</label>
																	<input type="text" class="form-control" name="speciality" value="<?=$list['speciality'] ?>" >
																</div>
																<div class="form-group col-md-6">
																	<label for="">Date of Birth</label>
																	<input type="date" data-date="" data-date-format="MMMM DD YYYY" value="<?=$list['dob'] ?>" class="form-control" name="dob" placeholder="">
																</div>
															</div>
															<div class="form-row">
																<div class="form-group col-md-6">
																	<label for=""> User Email ID </label>
																	<input type="text" class="form-control" name="emailid" value="<?=$list['empEmail'] ?>" readonly>
																</div>
																<div class="form-group col-md-6">
																	<label for=""> User Login Name </label>
																	<input type="text" class="form-control" name="login_name" value="<?=$list['userLoginName'] ?>" >
																</div>
															</div>
															<div class="form-row">
																<div class="form-group col-md-6">
																	<label for="">Password </label>
																	<input type="password" class="form-control" name="password" value="<?=$list['password'] ?>" >
																</div>
															</div>
														  <div class="form-group text-center">
														     <button type="submit" class="btn btn-primary center-block btn-lg addstu_btn"> Submit </button>
														  </div>
													</div>
												
										</div>
										</form>
									</div>
									
								</div>
							</div>
		<!-------- Add Installer End Modal -------->
				
											<tr>
												<th scope="row"><?=$list['empId'] ?></th>
												<td><?=$list['empName'] ?></td>
												<td><?=$list['empMobNumber'] ?></td>
												<td><?=$list['empEmail'] ?></td>
												<td>17</td>
												<td>7</td>
												<td>
													<a href="" data-toggle="modal" data-target="#editinstaller1<?=$list['id']?>" ><i class=" align-middle mr-2 fas fa-eye"></i></a>
													<!--<a href=""><i class="align-middle mr-2 far fa-fw fa-trash-alt"></i></a>-->
													<a href="" data-toggle="modal" data-target="#editinstaller<?=$list['id']?>" ><i class="align-middle fas fa-fw fa-edit"></i></a>
													
												</td>
											</tr>
										<?php }}else{?>
											
											<tr>
												<td scope="row" colspan="9"><center>No record Found !</center></td>
											</tr>
											 
										<?php } ?>
										</tbody>
									</table>


								</div>
						
						</div>
				</div>
			</main>

			<!-- <footer class="footer"></footer> -->
		</div>
	</div>
<!-- <script src="js\settings.js"></script> -->
	<script src="<?php echo base_url(); ?>/public/assets/js/app.js"></script>
	<script  src="<?php echo base_url(); ?>/public/assets/js/upload-img.js"></script>
	
</body>

</html>