<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="Bootlab">
	<title> Simon Says </title>
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,500;1,600;1,700;1,800;1,900&family=Nunito+Sans:ital,wght@0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/public/assets/css/classic.css">
</head>

<body>
	<main class="main conexbg">
		<div class="corner-img"><img src="<?php echo base_url(); ?>/public/assets/images/corner-img.png" class="img-fluid"></div>
		<div class="container">
			<div class="row height100vh align-items-center justify-content-center">
				<div class="col-sm-10 col-md-8 col-lg-7">
					<div class="login-logo-img">
						<h1 class="h2 loghed"><img src="<?php echo base_url(); ?>/public/assets/images/logo-big.png"></h1>
					</div>
					<div class="card">
							<div class="card-body">
									<div id="loginpage">
									<?php 
												if(!empty($msg))
												{
													echo '<div class="alert alert-danger" role="alert">'.$msg.'</div>';
												} 	
											?>
										<form action="<?php echo base_url('/admin/login_check'); ?>" method="post">
											<div class="form-group mail-icon">
												<input class="form-control form-control-lg" type="email" name="email" placeholder="Enter Registered Email*">
											</div>
											<div class="form-group passw">
												<input class="form-control form-control-lg" type="password" name="password" placeholder="Password*">
											</div>
											<div class="text-center mt-3">
											<button type="submit" class="btn btn-lg btn-primary logbtn">Login</a></button>
											</div>
										</form>
									</div>
							</div>
						</div>
				</div>
			</div>
		</div>
	</main>

	<script src="<?php echo base_url(); ?>/public/assets/js/app.js"></script>

</body>

</html>