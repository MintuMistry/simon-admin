
			<main class="content dashboard-box">
				<!--<div class="form-row justify-content-md-end pb-3 bordrstyle">
					<div class="form-group col-lg-4">
							<div class="input-group">
								<input type="text" class="form-control" placeholder="Search by Name and Emp ID.">
								<span class="input-group-append">
				                  <button class="btn btn-info" type="button">Go!</button>
				                </span>
							</div>
					</div>
					<div class="form-group col-lg-2">
						<div class="unblock-filter">
						   <select class="custom-select select-height">
							  <option selected="">Block</option>
							  <option value="1">Unblock</option>
							</select>
						</div>
					</div>
					<div class="form-group col-lg-2">
						<div class="btn import_btn w-100" data-toggle="modal" data-target="#add-installer-pop"><i class="align-middle mr-2 fas fa-fw fa-plus"></i> Add User </div>
					</div>

				</div>-->

				<div class="container-fluid p-0">
					<div class="row">
									<div class="col-12 col-lg-12 mt-minus">
									<div class="table-responsive  border">
									<table class="table mb-0">
										<thead>
											<tr>
												<th scope="col">Org ID </th>
												<th scope="col" style="">Org Name</th>
												<th scope="col" style="">Org Service Category</th>
												<th scope="col" style="">Org Email ID</th>
												<th scope="col" style="">Org Address</th>
												<th scope="col" style="">Org City</th>
												<th scope="col" style="">Org State</th>
												<th scope="col" style="">Org Country</th>
												<th scope="col">Action</th>
											</tr>
										</thead>
										<tbody>
										<?php 
										if(!empty($org_list))
										{
											foreach($org_list as $list){
										?>
										
								<!--------------- Edit org Modal ---------------->
			     			<div class="modal fade show" id="editorg<?=$list['orgId']?>" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h3 class="modal-title"> Edit Organisation</h3>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						                      <span aria-hidden="true">×</span>
						                    </button>
										</div>
											<div class="modal-body">
											<form action="<?php echo base_url();?>/admin/update_org/<?php echo $list['orgId']; ?>" method="post">
														<div id="formbox">
														  <div class="form-row">
														    <div class="form-group col-md-6">
														      <label for=""> Org Name </label>
														      <input type="text" class="form-control" name="orgName" placeholder="Org Name" value="<?=$list['orgName'] ?>" >
														    </div>
														    <div class="form-group col-md-6">
														      <label for="">Org Service Category</label>
														      <input type="text" class="form-control" name="service_category" placeholder="Org Service Category" value="<?=$list['orgServiceCategory'] ?>" >
														    </div>
														  </div>
														   <div class="form-row">
														   	<div class="form-group col-md-6">
														      <label for="">Org Email</label>
														      <input type="text" class="form-control" name="email" placeholder="Org Email" value="<?=$list['orgEmail'] ?>" value="<?=$list['orgEmail'] ?>" readonly>
														    </div>
														    <div class="form-group col-md-6">
														      <label for="">Org Address </label>
														      <input type="text" class="form-control" name="address" placeholder="Address" value="<?=$list['orgAddress'] ?>" >
														    </div>
														  </div>
														   <div class="form-row">
																<div class="form-group col-md-6">
																	<label for="inputAddress">Org City </label>
																	<input type="text" class="form-control" name="city" placeholder="City Name" value="<?=$list['orgCity'] ?>" >
																</div>
																<div class="form-group col-md-6">
																	<label for="inputAddress2">Org State</label>
																	<input type="text" class="form-control" name="state" placeholder="State Name" value="<?=$list['orgState'] ?>" >
																</div>
															</div>
															
															<div class="form-row">
																<div class="form-group col-md-6">
																	<label for="inputZip">Org Country </label>
																	<input type="text" class="form-control" name="country" placeholder="Country Name" value="<?=$list['orgCountry'] ?>" >
																</div>
																
															</div>
														  <div class="form-group text-center">
														     <button type="submit" class="btn btn-primary center-block btn-lg addstu_btn"> Submit </button>
														  </div>
													</div>
												</form>
										</div>
										
									</div>
								</div>
							</div>
		<!-------- Add Installer End Modal -------->


		<!--------------- View Installer Modal ---------------->

			     			<div class="modal fade show" id="vieworg<?=$list['orgId']?>" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h3 class="modal-title"> View Organisation</h3>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						                      <span aria-hidden="true">×</span>
						                    </button>
										</div>
										<div class="modal-body">
											<form>
														<div id="formbox">
														  <div class="form-row">
														    <div class="form-group col-md-6">
														      <label for=""> Org Name </label>
														      <input type="text" class="form-control" id="" placeholder="Org Name" value="<?=$list['orgName'] ?>" readonly>
														    </div>
														    <div class="form-group col-md-6">
														      <label for="">Org Service Category</label>
														      <input type="text" class="form-control" id="" placeholder="Org Service Category" value="<?=$list['orgServiceCategory'] ?>" readonly>
														    </div>
														  </div>
														   <div class="form-row">
														   	<div class="form-group col-md-6">
														      <label for="">Org Email</label>
														      <input type="text" class="form-control" id="" placeholder="Org Email" value="<?=$list['orgEmail'] ?>" value="<?=$list['orgEmail'] ?>" readonly>
														    </div>
														    <div class="form-group col-md-6">
														      <label for="">Org Address </label>
														      <input type="text" class="form-control" id="" placeholder="Address" value="<?=$list['orgAddress'] ?>" readonly>
														    </div>
														  </div>
														   <div class="form-row">
																<div class="form-group col-md-6">
																	<label for="inputAddress">Org City </label>
																	<input type="text" class="form-control" id="" placeholder="City Name" value="<?=$list['orgCity'] ?>" readonly>
																</div>
																<div class="form-group col-md-6">
																	<label for="inputAddress2">Org State</label>
																	<input type="text" class="form-control" id="" placeholder="State Name" value="<?=$list['orgState'] ?>" readonly>
																</div>
															</div>
															
															<div class="form-row">
																<div class="form-group col-md-6">
																	<label for="inputZip">Org Country </label>
																	<input type="text" class="form-control" id="" placeholder="Country Name" value="<?=$list['orgCountry'] ?>" readonly>
																</div>
																
															</div>
													</div>
													</form>
										</div>
										
									</div>
								</div>
							</div>
				<!-------- View Installer End Modal -------->
				
				
											<tr>
												<th scope="row"><?=$list['orgId'] ?></th>
												<td><?=$list['orgName'] ?></td>
												<td><?=$list['orgServiceCategory'] ?></td>
												<td><?=$list['orgEmail'] ?></td>
												<td><?=$list['orgAddress'] ?></td>
												<td><?=$list['orgCity'] ?></td>
												<td><?=$list['orgState'] ?></td>
												<td><?=$list['orgCountry'] ?></td>
												<td>
													<a href="" data-toggle="modal" data-target="#vieworg<?=$list['orgId']?>"><i class=" align-middle mr-2 fas fa-eye"></i></a>
													<!--<a href=""><i class="align-middle mr-2 far fa-fw fa-trash-alt"></i></a>-->
													<a href="" data-toggle="modal" data-target="#editorg<?=$list['orgId']?>" ><i class="align-middle fas fa-fw fa-edit"></i></a>
													
												</td>
											</tr>
										<?php }}else{?>
											
											<tr>
												<td scope="row" colspan="9"><center>No record Found !</center></td>
											</tr>
											 
										<?php } ?>
										</tbody>
									</table>


								</div>
						
						</div>
				</div>
			</main>

			<!-- <footer class="footer"></footer> -->
		</div>
	</div>
<!-- <script src="js\settings.js"></script> -->
	<script src="<?php echo base_url(); ?>/public/assets/js/app.js"></script>
	<script  src="<?php echo base_url(); ?>/public/assets/js/upload-img.js"></script>
	
</body>

</html>