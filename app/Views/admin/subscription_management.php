﻿
			<main class="content dashboard-box">
				<div class="form-row justify-content-md-end">
					<div class="form-group col-lg-4">
						<div class="form-row">
							<div class="col-lg-6">
								<div class="form-group date-name">
									<span>Start Date</span>
								 	<input type="date" name="bday" min="1000-01-01" max="3000-12-31" class="form-control">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group date-name">
									<span>End Date</span>
								 	<input type="date" name="bday" min="1000-01-01" max="3000-12-31" class="form-control">
								</div>
							</div>
						</div>
					</div>

					<div class="form-group col-lg-4">
							<div class="input-group">
								<input type="text" class="form-control" placeholder="Search by Order Id/Customer Name">
								<span class="input-group-append">
				                  <button class="btn btn-info" type="button">Go!</button>
				                </span>
							</div>
					</div>
					<div class="form-group col-lg-2">
						<div class="unblock-filter">
						   <select class="custom-select select-height">
							  <option selected="">Filter</option>
							  <option value="1">Dummy text</option>
							  <option value="2">Dummy text</option>
							  <option value="3">Dummy text</option>
							</select>
						</div>
					</div>
					
				</div>


		<!--------------- Complete Job Modal ---------------->

			     			<div class="modal fade show" id="complete-job-pop" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h3 class="modal-title"> View Job History (Complete Job) </h3>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						                      <span aria-hidden="true">×</span>
						                    </button>
										</div>
										<div class="modal-body">
											<form>
												<div id="formbox">
												  <div class="form-row">
												    <div class="form-group col-md-6">
												      <label for=""> Order ID </label>
												      <input type="text" class="form-control" id="" placeholder="DF654" readonly>
												    </div>
												    <div class="form-group col-md-6">
												      <label for=""> Job Type</label>
												      <input type="text" class="form-control" id="" placeholder="Commercial" readonly>
												    </div>
												  </div>
												   <div class="form-row">
												   	<div class="form-group col-md-6">
												      <label for="">Customer Name</label>
												      <input type="text" class="form-control" id="" placeholder="Barry J Smith" readonly>
												    </div>
												    <div class="form-group col-md-6">
												      <label for="">Area</label>
												      <input type="text" class="form-control" id="" placeholder="500sqf." readonly>
												    </div>
												  </div>
												   <div class="form-row">
														<div class="form-group col-md-6">
															<label for="inputAddress">Date and Time</label>
															<input type="text" class="form-control" id="" placeholder="12/16/2020 4:37 PM" readonly>
														</div>
														<div class="form-group col-md-6">
															<label for="inputAddress2">Salesperson </label>
															<input type="text" class="form-control" id="" placeholder="Chad Smith" readonly>
														</div>
													</div>
													<div class="form-row">
														<div class="form-group col-md-6">
															<label for="inputCity">Project Manager</label>
															<input type="text" class="form-control" id="" placeholder="Adam J Johnson" readonly>
														</div>
														<div class="form-group col-md-6">
															<label for="inputState">Price</label>
															<input type="text" class="form-control" id="" placeholder="$200" readonly>
														</div>
													</div>
													<div class="form-row">
														<div class="form-group col-md-6">
															<label for="inputZip">Installer Name</label>
															 <input type="text" class="form-control" id="" placeholder="Marcus D" readonly>
														</div>
														<div class="form-group col-md-6">
															<label for="inputZip">Status</label>
															 <input type="text" class="form-control" id="" placeholder="Complete" readonly>
														</div>
													</div>
													 <div class="form-group">
													    <label for="">Note for installer</label>
													    <textarea class="form-control" id="" rows="3" placeholder="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus." readonly></textarea>
													  </div>
													  <div class="form-group">
													    <label for="">Client Feedback</label>
													   <div class="client-fb-area">
													   		<div class="row align-items-center">
													   			<div class="col-8">
													   				<div class="fd-name">
													   					<p><b>Name : </b> Isabella </p>
													   					<h6><b>Initials type : </b> Dummy text </h6>
													   				</div>
													   			</div>
													   			<div class="col-4">
													   				<div class="signat"><img src="img/signature.png" alt="signature" class="img-fluid"></div>
													   			</div>
													   		</div>
													   </div>
													  </div>
													</div>
												</form>
										</div>
										
									</div>
								</div>
							</div>
				<!-------- End Modal -------->
	<!--------------- View Incomplete Job Modal ---------------->

			     			<div class="modal fade show" id="incomplete-job-pop" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h3 class="modal-title"> View Job History (Incomplete Job) </h3>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						                      <span aria-hidden="true">×</span>
						                    </button>
										</div>
										<div class="modal-body">
											<form>
												<div id="formbox">
												  <div class="form-row">
												    <div class="form-group col-md-6">
												      <label for=""> Order ID </label>
												      <input type="text" class="form-control" id="" placeholder="DF654" readonly>
												    </div>
												    <div class="form-group col-md-6">
												      <label for=""> Job Type</label>
												      <input type="text" class="form-control" id="" placeholder="Commercial" readonly>
												    </div>
												  </div>
												   <div class="form-row">
												   	<div class="form-group col-md-6">
												      <label for="">Customer Name</label>
												      <input type="text" class="form-control" id="" placeholder="Barry J Smith" readonly>
												    </div>
												    <div class="form-group col-md-6">
												      <label for="">Area</label>
												      <input type="text" class="form-control" id="" placeholder="500sqf." readonly>
												    </div>
												  </div>
												   <div class="form-row">
														<div class="form-group col-md-6">
															<label for="inputAddress">Date and Time</label>
															<input type="text" class="form-control" id="" placeholder="12/16/2020 4:37 PM" readonly>
														</div>
														<div class="form-group col-md-6">
															<label for="inputAddress2">Salesperson </label>
															<input type="text" class="form-control" id="" placeholder="Chad Smith" readonly>
														</div>
													</div>
													<div class="form-row">
														<div class="form-group col-md-6">
															<label for="inputCity">Project Manager</label>
															<input type="text" class="form-control" id="" placeholder="Adam J Johnson" readonly>
														</div>
														<div class="form-group col-md-6">
															<label for="inputState">Price</label>
															<input type="text" class="form-control" id="" placeholder="$200" readonly>
														</div>
													</div>
													<div class="form-row">
														<div class="form-group col-md-6">
															<label for="inputZip">Installer Name</label>
															 <input type="text" class="form-control" id="" placeholder="Marcus D" readonly>
														</div>
														<div class="form-group col-md-6">
															<label for="inputZip">Status</label>
															 <input type="text" class="form-control" id="" placeholder="Incomplete" readonly>
														</div>
													</div>
													 <div class="form-group">
													    <label for="">Note for installer</label>
													    <textarea class="form-control" id="" rows="3" placeholder="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus." readonly></textarea>
													  </div>
													  <div class="yes-no">
													  	<p>Are there any issues, missing materials, or items that need completed on this job?</p>
													  	<div class="yesbox"> Yes </div>
													  </div>
													  <div class="form-group">
													    <label for="">Feedback</label>
													   <textarea class="form-control" id="" rows="3" placeholder="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus." readonly></textarea>
													  </div>
													  <div class="yes-no">
													  	<p>Do we need to return to this job for any reason?</p>
													  	<div class="yesbox"> No </div>
													  </div>
													</div>
												</form>
										</div>
										
									</div>
								</div>
							</div>
				<!-------- View Installer End Modal -------->
				<div class="container-fluid p-0">
					<div class="row">
									<div class="col-12 col-lg-12 mt-minus">
									<div class="table-responsive  border">
									<table class="table mb-0">
										<thead>
											<tr>
												<th class="">Order ID </th>
												<th class="">Job type </th>
												<th class="name-t-size">Customer Name</th>
												<th class="">Area</th>
												<th class="name-t-size">Salesperson</th>
												<th class="name-t-size">Date and time</th>
												<th class="">Price</th>
												<th class="name-t-size">Project Manager</th>
												<th class="name-t-size">Installer</th>
												<th class="text-center">Job Status</th>
												<th class="">Action</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<th scope="row">DF654</th>
												<td>Commercial</td>
												<td>Chad Smith</td>
												<td>300Sft</td>
												<td>Barry J Smith</td>
												<td>10/22/2021 <span> (10:40) </span></td>
												<td>$500</td>
												<td>David R Smith</td>
												<td>A Smith</td>
												<td class="job-box cmplt">Complete Job</td>
												<td class="text-center">
													<a href="" data-toggle="modal" data-target="#complete-job-pop"><i class=" align-middle mr-2 fas fa-eye"></i></a>
												</td>
											</tr>
											<tr>
												<th scope="row">DF654</th>
												<td>Residental</td>
												<td>Chad Smith</td>
												<td>300Sft</td>
												<td>Barry J Smith</td>
												<td>10/22/2021 <span> (10:40) </span></td>
												<td>$500</td>
												<td>David R Smith</td>
												<td>A Smith</td>
												<td class="job-box incmplt">Incomplete job</td>
												<td class="text-center">
													<a href="" data-toggle="modal" data-target="#incomplete-job-pop"><i class=" align-middle mr-2 fas fa-eye"></i></a>
												</td>
											</tr>
											<tr>
												<th scope="row">DF654</th>
												<td>Commercial</td>
												<td>Chad Smith</td>
												<td>300Sft</td>
												<td>Barry J Smith</td>
												<td>10/22/2021 <span> (10:40) </span></td>
												<td>$500</td>
												<td>David R Smith</td>
												<td>A Smith</td>
												<td class="job-box notavil">Customer Not Available</td>
												<td class="text-center">
													<a href=""><i class=" align-middle mr-2 fas fa-eye"></i></a>
												</td>
											</tr>
											<tr>
												<th scope="row">DF654</th>
												<td>Residental</td>
												<td>Chad Smith</td>
												<td>300Sft</td>
												<td>Barry J Smith</td>
												<td>10/22/2021 <span> (10:40) </span></td>
												<td>$500</td>
												<td>David R Smith</td>
												<td>A Smith</td>
												<td class="job-box cmplt">Complete Job</td>
												<td class="text-center">
													<a href=""><i class=" align-middle mr-2 fas fa-eye"></i></a>
												</td>
											</tr>
											<tr>
												<th scope="row">DF654</th>
												<td>Residental</td>
												<td>Chad Smith</td>
												<td>300Sft</td>
												<td>Barry J Smith</td>
												<td>10/22/2021 <span> (10:40) </span></td>
												<td>$500</td>
												<td>David R Smith</td>
												<td>A Smith</td>
												<td class="job-box notavil">Customer Not Available</td>
												<td class="text-center">
													<a href=""><i class=" align-middle mr-2 fas fa-eye"></i></a>
												</td>
											</tr>
											<tr>
												<th scope="row">DF654</th>
												<td>Residental</td>
												<td>Chad Smith</td>
												<td>300Sft</td>
												<td>Barry J Smith</td>
												<td>10/22/2021 <span> (10:40) </span></td>
												<td>$500</td>
												<td>David R Smith</td>
												<td>A Smith</td>
												<td class="job-box incmplt">Incomplete job</td>
												<td class="text-center">
													<a href=""><i class=" align-middle mr-2 fas fa-eye"></i></a>
												</td>
											</tr>
											<tr>
												<th scope="row">DF654</th>
												<td>Residental</td>
												<td>Chad Smith</td>
												<td>300Sft</td>
												<td>Barry J Smith</td>
												<td>10/22/2021 <span> (10:40) </span></td>
												<td>$500</td>
												<td>David R Smith</td>
												<td>A Smith</td>
												<td class="job-box cmplt">Complete Job</td>
												<td class="text-center">
													<a href=""><i class=" align-middle mr-2 fas fa-eye"></i></a>
												</td>
											</tr>
											
										</tbody>
									</table>


								</div>
						
						</div>
				</div>
			</main>

			<!-- <footer class="footer"></footer> -->
		</div>
	</div>
<!-- <script src="js\settings.js"></script> -->
	<script src="<?php echo base_url(); ?>/public/assets/js/app.js"></script>
	<script  src="<?php echo base_url(); ?>/public/assets/js/upload-img.js"></script>
	
</body>

</html>