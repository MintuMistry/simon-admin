﻿<main class="content dashboard-box">
<div class="container-fluid p-0">
<div class="row">
<div class="col-12 col-lg-12 mt-minus">
<div class="table-responsive  border">
<table class="table mb-0">
	<thead>
		<tr>
			<th class="">Plan Name </th>
			<th class="">Amount Per User ($) </th>
			<th class="">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach($subscription_plan_list as $list){?>
		
				<!--------------- Edit support Modal ---------------->
		<div class="modal fade show" id="editSupport<?=$list['plan_id']?>" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
					   <h3 class="modal-title"> Update Subscription Plan </h3>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  <span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body">
								<form action="<?php echo base_url();?>/admin/update_subscription_plan/<?php echo $list['plan_id']; ?>" method="post">
									<div id="formbox">
									  
									   <div class="form-row">
										<div class="form-group col-md-6">
										  <label for="">Plan Name</label>
										  <input type="text" class="form-control" name="PName" placeholder="" value="<?=$list['plan_name']?>">
										</div>
										<div class="form-group col-md-6">
										  <label for="">Amount Per User ($)</label>
										  <input type="text" class="form-control" name="AmountPUser" placeholder="" value="<?=$list['amountPerUser']?>">
										</div>
									  </div>
									  <div class="form-group text-center">
										 <button type="submit" class="btn btn-primary center-block btn-lg addstu_btn"> Submit </button>
									  </div>
								</div>
							</form>
					</div>
					
				</div>
			</div>
		</div>
<!-------- Edit support End Modal -------->

		<tr>
			<td><?=$list['plan_name']?></td>
			<td><?=$list['amountPerUser']?></td>
			<td>
				<a href="" data-toggle="modal" data-target="#editSupport<?=$list['plan_id']?>" ><i class="align-middle fas fa-fw fa-edit"></i></a>
			</td>
			
		</tr>
		
	<?php }?>
	</tbody>
</table>


</div>
	
	</div>
</div>
</main>

			<!-- <footer class="footer"></footer> -->
</div>
	</div>
<!-- <script src="js\settings.js"></script> -->
	<script src="<?php echo base_url(); ?>/public/assets/js/app.js"></script>
	<script  src="<?php echo base_url(); ?>/public/assets/js/upload-img.js"></script>
	
</body>

</html>