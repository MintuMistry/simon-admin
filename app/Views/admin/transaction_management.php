﻿

			<main class="content dashboard-box">

				<div class="container-fluid p-0">
					<div class="row">
									<div class="col-12 col-lg-12 mt-minus">
									<div class="table-responsive  border">
									<table class="table mb-0">
										<thead>
											<tr>
												<th scope="col">Sr No </th>
												<th scope="col" style="">Organissation Name</th>
												<th scope="col" style="">Transaction Id</th>
												<th scope="col" style="">Plan Name</th>
												<th scope="col" style="">Amount</th>
												<th scope="col" style="">Transaction Date</th>
											</tr>
										</thead>
										<tbody>
										<?php
										if(!empty($transaction_list))
										{
											$i=0;
											foreach($transaction_list as $list){
												$i++;
										?>
											<tr>
												<th scope="row"><?=$i?></th>
												<td><?=$list['orgid']?></td>
												<td><?=$list['txn_id']?></td>
												<td><?=$list['planid']?></td>
												<td><?=$list['amount']?></td>
												<td><?php echo date('m-d-y', strtotime($list['createdat'])); ?>	</td>
											</tr>
										<?php }}else{?>
											<tr><td colspan="7"><center>No Record Found !</center></td></tr>
										<?php }
											?>
										</tbody>
									</table>


								</div>
						
						</div>
				</div>
			</main>

			<!-- <footer class="footer"></footer> -->
		</div>
	</div>
<!-- <script src="js\settings.js"></script> -->
	<script src="<?php echo base_url(); ?>/public/assets/js/app.js"></script>
	
</body>

</html>