﻿

			<main class="content dashboard-box">
				<div class="form-row justify-content-md-end">
					<div class="form-group col-lg-4">
						<div class="form-row">
							<div class="col-lg-6">
								<div class="form-group date-name">
									<span>Start Date</span>
								 	<input type="date" name="bday" min="1000-01-01" max="3000-12-31" class="form-control">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group date-name">
									<span>End Date</span>
								 	<input type="date" name="bday" min="1000-01-01" max="3000-12-31" class="form-control">
								</div>
							</div>
						</div>
					</div>

					<div class="form-group col-lg-4">
							<div class="input-group">
								<input type="text" class="form-control" placeholder="Search by Order Id/Customer Name">
								<span class="input-group-append">
				                  <button class="btn btn-info" type="button">Go!</button>
				                </span>
							</div>
					</div>
					<div class="form-group col-lg-2">
						<div class="unblock-filter">
						   <select class="custom-select select-height">
							  <option selected="">Filter</option>
							  <option value="1">Assign</option>
							  <option value="2">Unassigned</option>
							  <option value="3">Date</option>
							  <option value="4">Price</option>
							  <option value="5">Job type</option>
							</select>
						</div>
					</div>
					<div class="form-group col-lg-2">
						<div class="btn import_btn w-100" data-toggle="modal" data-target="#create-job-pop"><i class="align-middle mr-2 fas fa-fw fa-plus"></i> Create Job </div>
					</div>

				</div>
				<div class="form-row justify-content-md-end">
					<div class="col-lg-5">
						<div class="btn-rightbox">
						<a class="btn googl-bnt" href="#" role="button"><img src="<?php echo base_url(); ?>/public/assets/images/google.png"> Connect with Google</a> 
						<a class="btn imp_exp-bnt" href="#" role="button">Import/Export Data</a></div>
					</div>
					
				</div>
		<!--------------- Add Installer Modal ---------------->
			     			<div class="modal fade show" id="create-job-pop" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
										   <h3 class="modal-title"> Create Job </h3>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						                      <span aria-hidden="true">×</span>
						                    </button>
										</div>
										<div class="modal-body">
													<form>
														<div id="formbox">
														  <div class="form-row">
														    <div class="form-group col-md-6">
														      <label for=""> Order ID </label>
														      <input type="text" class="form-control" id="" placeholder=" ">
														    </div>
														    <div class="form-group col-md-6">
														      <label for=""> Job Type</label>
														      <select class="custom-select">
																  <option selected="">Commercial</option>
																  <option value="1">Residential</option>
															  </select>
														    </div>
														  </div>
														   <div class="form-row">
														   	<div class="form-group col-md-6">
														      <label for="">Customer Name</label>
														      <input type="text" class="form-control" id="" placeholder="">
														    </div>
														    <div class="form-group col-md-6">
														      <label for="">Area</label>
														      <input type="text" class="form-control" id="" placeholder="">
														    </div>
														  </div>
														   <div class="form-row">
																<div class="form-group col-md-6">
																	<label for="inputAddress">Date and Time</label>
																	<input type="date" name="bday" min="1000-01-01" max="3000-12-31" class="form-control">
																</div>
																<div class="form-group col-md-6">
																	<label for="inputAddress2">Salesperson </label>
																	<input type="text" class="form-control" id="" placeholder="">
																</div>
															</div>
															<div class="form-row">
																<div class="form-group col-md-6">
																	<label for="inputCity">Project Manager</label>
																	 <select class="custom-select">
																  <option selected="">Project Manager-Name-1</option>
																  <option value="1">Project Manager-Name-2</option>
																  <option value="2">Project Manager-Name-3</option>
																  <option value="3">Project Manager-Name-4</option>
															  </select>
																</div>
																<div class="form-group col-md-6">
																	<label for="inputState">Price</label>
																	<input type="text" class="form-control" id="" placeholder="">
																</div>
															</div>
															<div class="form-row">
																<div class="form-group col-md-6">
																	<label for="inputZip">Service Provider / User Name</label>
																	 <select class="custom-select">
																		  <option selected="">User-Name-1</option>
																		  <option value="1">User-Name-2</option>
																		  <option value="2">User-Name-3</option>
																		  <option value="3">User-Name-4</option>
																	  </select>
																</div>
															</div>
															 <div class="form-group">
															    <label for="">Project Notes</label>
															    <textarea class="form-control" id="" rows="3"></textarea>
															  </div>
															  <div class="form-group">
															    <label for="">Description of Job</label>
															    <textarea class="form-control" id="" rows="3"></textarea>
															  </div>
															
														  <div class="form-group text-center">
														     <button type="submit" class="btn btn-primary center-block btn-lg addstu_btn"> Submit </button>
														  </div>
													</div>
												</form>
										</div>
										
									</div>
								</div>
							</div>
		<!-------- Add Installer End Modal -------->


		<!--------------- View Installer Modal ---------------->

			     			<div class="modal fade show" id="view-job-pop" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h3 class="modal-title"> </h3>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						                      <span aria-hidden="true">×</span>
						                    </button>
										</div>
										<div class="modal-body">
											<form>
												<div id="formbox">
												  <div class="form-row">
												    <div class="form-group col-md-6">
												      <label for=""> Order ID </label>
												      <input type="text" class="form-control" id="" placeholder="DF654" readonly>
												    </div>
												    <div class="form-group col-md-6">
												      <label for=""> Job Type</label>
												      <input type="text" class="form-control" id="" placeholder="Commercial" readonly>
												    </div>
												  </div>
												   <div class="form-row">
												   	<div class="form-group col-md-6">
												      <label for="">Customer Name</label>
												      <input type="text" class="form-control" id="" placeholder="Barry J Smith" readonly>
												    </div>
												    <div class="form-group col-md-6">
												      <label for="">Area</label>
												      <input type="text" class="form-control" id="" placeholder="500sqf." readonly>
												    </div>
												  </div>
												   <div class="form-row">
														<div class="form-group col-md-6">
															<label for="inputAddress2">Salesperson </label>
															<input type="text" class="form-control" id="" placeholder="Chad Smith" readonly>
															
														</div>
														<div class="form-group col-md-6">
															<label for="inputCity">Project Manager</label>
															<input type="text" class="form-control" id="" placeholder="Adam J Johnson" readonly>
														</div>
													</div>
													<div class="form-row">
														<div class="form-group col-md-6">
															<label for="inputZip">Installer Name</label>
															 <input type="text" class="form-control" id="" placeholder="Marcus D" readonly>
															
														</div>
														<div class="form-group col-md-6">
															<label for="inputAddress">Date and Time</label>
															<input type="text" class="form-control" id="" placeholder="12/16/2020 4:37 PM" readonly>
															
														</div>
													</div>
													<div class="form-row">
														<div class="form-group col-md-6">
															<label for="inputState">Price</label>
															<input type="text" class="form-control" id="" placeholder="$200" readonly>
														</div>
													</div>
													 <div class="form-group">
													    <label for="">Note for installer</label>
													    <textarea class="form-control" id="" rows="3" placeholder="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus." readonly></textarea>
													  </div>
													  <div class="form-group">
													    <label for="">Description of Job</label>
													    <textarea class="form-control" id="" rows="3" placeholder="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus." readonly></textarea>
													  </div>
													</div>
												</form>
										</div>
										
									</div>
								</div>
							</div>
				<!-------- View Installer End Modal -------->

				<div class="container-fluid p-0">
					<div class="row">
									<div class="col-12 col-lg-12 mt-minus">
									<div class="table-responsive  border">
									<table class="table mb-0">
										<thead>
											<tr>
												<th class="">Order ID </th>
												<th class="">Job type </th>
												<th class="name-t-size">Customer Name</th>
												<th class="">Area</th>
												<th class="name-t-size">Salesperson</th>
												<th class="name-t-size">Date and time</th>
												<th class="">Price</th>
												<th class="name-t-size">Project Manager</th>
												<th class="name-t-size">Assign Installer</th>
												<th class="">Status</th>
												<th class="">Action</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<th scope="row">DF654</th>
												<td>Commercial</td>
												<td>Chad Smith</td>
												<td>300Sft</td>
												<td>Barry J Smith</td>
												<td>10/22/2021 <span> (10:40) </span></td>
												<td>$500</td>
												
												<td>David R Smith</td>
												<td>A Smith</td>
												<td>Assign</td>
												<td>
													<a href="" data-toggle="modal" data-target="#view-job-pop"><i class=" align-middle mr-2 fas fa-eye"></i></a>
													<a href=""><i class="align-middle mr-2 fas fa-fw fa-pencil-alt"></i></a>
													<a href=""><i class="align-middle far fa-fw fa-trash-alt"></i></a>
												</td>
												
											</tr>
											<tr>
												<th scope="row">DF654</th>
												<td>Residental</td>
												<td>Chad Smith</td>
												<td>300Sft</td>
												<td>Barry J Smith</td>
												
												<td>10/22/2021 <span> (10:40) </span></td>
												<td>$500</td>
												<td>David R Smith</td>
												<td>A Smith</td>
												<td>Unassigned</td>
												<td>
													<a href="" data-toggle="modal" data-target="#view-job-pop"><i class=" align-middle mr-2 fas fa-eye"></i></a>
													<a href=""><i class="align-middle mr-2 fas fa-fw fa-pencil-alt"></i></a>
													<a href=""><i class="align-middle far fa-fw fa-trash-alt"></i></a>
												</td>
											</tr>
											<tr>
												<th scope="row">DF654</th>
												<td>Commercial</td>
												<td>Chad Smith</td>
												<td>300Sft</td>
												<td>Barry J Smith</td>
												
												<td>10/22/2021 <span> (10:40) </span></td>
												<td>$500</td>
												<td>David R Smith</td>
												<td>A Smith</td>
												<td>Assign</td>
												<td>
													<a href="" data-toggle="modal" data-target="#view-job-pop"><i class=" align-middle mr-2 fas fa-eye"></i></a>
													<a href=""><i class="align-middle mr-2 fas fa-fw fa-pencil-alt"></i></a>
													<a href=""><i class="align-middle far fa-fw fa-trash-alt"></i></a>
												</td>
											</tr>
											<tr>
												<th scope="row">DF654</th>
												<td>Residental</td>
												<td>Chad Smith</td>
												<td>300Sft</td>
												<td>Barry J Smith</td>
												
												<td>10/22/2021 <span> (10:40) </span></td>
												<td>$500</td>
												<td>David R Smith</td>
												<td>A Smith</td>
												<td>Unassigned</td>
												<td>
													<a href="" data-toggle="modal" data-target="#view-job-pop"><i class=" align-middle mr-2 fas fa-eye"></i></a>
													<a href=""><i class="align-middle mr-2 fas fa-fw fa-pencil-alt"></i></a>
													<a href=""><i class="align-middle far fa-fw fa-trash-alt"></i></a>
												</td>
											</tr>
											<tr>
												<th scope="row">DF654</th>
												<td>Residental</td>
												<td>Chad Smith</td>
												<td>300Sft</td>
												<td>Barry J Smith</td>
												
												<td>10/22/2021 <span> (10:40) </span></td>
												<td>$500</td>
												<td>David R Smith</td>
												<td>A Smith</td>
												<td>Assign</td>
												<td>
													<a href="" data-toggle="modal" data-target="#view-job-pop"><i class=" align-middle mr-2 fas fa-eye"></i></a>
													<a href=""><i class="align-middle mr-2 fas fa-fw fa-pencil-alt"></i></a>
													<a href=""><i class="align-middle far fa-fw fa-trash-alt"></i></a>
												</td>
											</tr>
											<tr>
												<th scope="row">DF654</th>
												<td>Residental</td>
												<td>Chad Smith</td>
												<td>300Sft</td>
												<td>Barry J Smith</td>
												
												<td>10/22/2021 <span> (10:40) </span></td>
												<td>$500</td>
												<td>David R Smith</td>
												<td>A Smith</td>
												<td>Assign</td>
												<td>
													<a href="" data-toggle="modal" data-target="#view-job-pop"><i class=" align-middle mr-2 fas fa-eye"></i></a>
													<a href=""><i class="align-middle mr-2 fas fa-fw fa-pencil-alt"></i></a>
													<a href=""><i class="align-middle far fa-fw fa-trash-alt"></i></a>
												</td>
											</tr>
											<tr>
												<th scope="row">DF654</th>
												<td>Residental</td>
												<td>Chad Smith</td>
												<td>300Sft</td>
												<td>Barry J Smith</td>
											
												<td>10/22/2021 <span> (10:40) </span></td>
												<td>$500</td>
												<td>David R Smith</td>
												<td>A Smith</td>
												<td>Assign</td>
												<td>
													<a href="" data-toggle="modal" data-target="#view-job-pop"><i class=" align-middle mr-2 fas fa-eye"></i></a>
													<a href=""><i class="align-middle mr-2 fas fa-fw fa-pencil-alt"></i></a>
													<a href=""><i class="align-middle far fa-fw fa-trash-alt"></i></a>
												</td>
											</tr>
											
										</tbody>
									</table>


								</div>
						
						</div>
				</div>
			</main>

			<!-- <footer class="footer"></footer> -->
		</div>
	</div>
<!-- <script src="js\settings.js"></script> -->
	<script src="<?php echo base_url(); ?>/public/assets/js/app.js"></script>
	<script  src="<?php echo base_url(); ?>/public/assets/js/upload-img.js"></script>
	
</body>

</html>