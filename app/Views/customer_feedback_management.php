﻿

			<main class="content dashboard-box">
				<div class="container p-0 bg-white">
					<div class="row justify-content-center">
						<div class="col-12 col-lg-7 p-3">
							<div class="card radious-0">
								<div class="card-body h-100 p-lg-0">
									<div class="row justify-content-center">
										<div class="col-lg-5 p-lg-0 border-lg mr-onepx">
											<div class="app-img"><img src="<?php echo base_url(); ?>/public/assets/images/app-img.png" alt="app img" class="img-fluid"></div>
										</div>
										<div class="col-lg-6 p-lg-0 border-lg">
											<div class="feedback-box bg-gray">
												<div class="feedback">Customer <br> Feedback Management</div>
												<form>
												  <div class="form-group titl">
												    <label for="">Title</label>
												    <input type="text" class="form-control" id="" aria-describedby="">
												    </div>
												  <div class="form-group descrip">
												    <label for="exampleInputPassword1">Descripton</label>
												    <textarea class="form-control" id="" rows="3"></textarea>
												  </div>
												  <button type="submit" class="btn custm-fb-btn">Submit</button>
												  <p class="note-box"> <b> Note: </b> The name of the organization with the 
													logo on this screen comes from the profile 
													you filled out.</p>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>

			<!-- <footer class="footer"></footer> -->
		</div>
	</div>
<!-- <script src="js\settings.js"></script> -->
	<script src="<?php echo base_url(); ?>/public/assets/js/app.js"></script>
	<script  src="<?php echo base_url(); ?>/public/assets/js/upload-img.js"></script>
	
</body>

</html>