﻿
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
  
</button>
<?php $this->session = \Config\Services::session()?>
<?php if($this->session->success_msg){ ?>
	<div class="alert alert-success alert-dismissible fade in">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
	<strong>Success!  </strong><?php echo $this->session->success_msg; ?></div>
<?php } if($this->session->error_msg){?>
<div class="alert alert-success alert-dismissible fade in">
<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
<strong>Fail!   </strong><?php echo $this->session->error_msg; ?></div>
<?php } ?>			
			<main class="content dashboard-box">
				<div class="container-fluid p-0">
					<div class="row">
						<div class="col-sm-3">
							<div class="card dasbox">
								<div class="card-header">
									<h5 class="dash-txt"> Number of installers </h5>
								</div>
								<h2 class="dash-number"> 1721 </h2>
								<div class="progress progress-sm shadow-sm mb-1">
										<div class="progress-bar bg-warning" role="progressbar" style="width: 57%"></div>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="card dasbox">
								<div class="card-header">
									<h5 class="dash-txt"> Number of project manager </h5>
								</div>
								<h2 class="dash-number"> 123 </h2>
								<div class="progress progress-sm shadow-sm mb-1">
										<div class="progress-bar bg-red" role="progressbar" style="width: 82%"></div>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="card dasbox">
								<div class="card-header">
									<h5 class="dash-txt"> Number of the job completed </h5>
								</div>
								<h2 class="dash-number"> 120 </h2>
								<div class="progress progress-sm shadow-sm mb-1">
										<div class="progress-bar bg-green" role="progressbar" style="width: 57%"></div>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="card dasbox">
								<div class="card-header">
									<h5 class="dash-txt"> Number of job incomplete </h5>
								</div>
								<h2 class="dash-number"> 17 </h2>
								<div class="progress progress-sm shadow-sm mb-1">
										<div class="progress-bar bg-primary" role="progressbar" style="width: 87%"></div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-12 col-lg-12 d-flex">
							<div class="card flex-fill w-100">
								<div class="card-header p-2">
									<span class="badge badge-primary float-right">Monthly</span>
									<h5 class="card-title mb-0">Total</h5>
								</div>
								<div class="card-body">
									<div class="chart chart-lg"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
										<canvas id="chartjs-dashboard-line" width="625" height="350" class="chartjs-render-monitor" style="display: block; width: 625px; height: 350px;"></canvas>
									</div>
								</div>
							</div>
						</div>
						
					</div>

				</div>
			</main>

			<!-- <footer class="footer"></footer> -->
		</div>
	</div>
<!-- <script src="js\settings.js"></script> -->
	<script src="<?php echo base_url(); ?>/public/assets/js/app.js"></script>
	<script>
		$(function() {
			// Bar chart
			new Chart(document.getElementById("chartjs-dashboard-bar"), {
				type: "bar",
				data: {
					labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
					datasets: [{
						label: "Last year",
						backgroundColor: window.theme.primary,
						borderColor: window.theme.primary,
						hoverBackgroundColor: window.theme.primary,
						hoverBorderColor: window.theme.primary,
						data: [54, 67, 41, 55, 62, 45, 55, 73, 60, 76, 48, 79]
					}, {
						label: "This year",
						backgroundColor: "#E8EAED",
						borderColor: "#E8EAED",
						hoverBackgroundColor: "#E8EAED",
						hoverBorderColor: "#E8EAED",
						data: [69, 66, 24, 48, 52, 51, 44, 53, 62, 79, 51, 68]
					}]
				},
				options: {
					maintainAspectRatio: false,
					legend: {
						display: false
					},
					scales: {
						yAxes: [{
							gridLines: {
								display: false
							},
							stacked: false,
							ticks: {
								stepSize: 20
							}
						}],
						xAxes: [{
							barPercentage: .75,
							categoryPercentage: .5,
							stacked: false,
							gridLines: {
								color: "transparent"
							}
						}]
					}
				}
			});
		});
	</script>
	<script>
		$(function() {
			$("#datetimepicker-dashboard").datetimepicker({
				inline: true,
				sideBySide: false,
				format: "L"
			});
		});
	</script>
	<script>
		$(function() {
			// Line chart
			new Chart(document.getElementById("chartjs-dashboard-line"), {
				type: "line",
				data: {
					labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
					datasets: [{
						label: "Sales ($)",
						fill: true,
						backgroundColor: "transparent",
						borderColor: window.theme.primary,
						data: [2015, 1465, 1487, 1796, 1387, 2123, 2866, 2548, 3902, 4938, 3917, 4927]
					}, {
						label: "Orders",
						fill: true,
						backgroundColor: "transparent",
						borderColor: window.theme.tertiary,
						borderDash: [4, 4],
						data: [928, 734, 626, 893, 921, 1202, 1396, 1232, 1524, 2102, 1506, 1887]
					}]
				},
				options: {
					maintainAspectRatio: false,
					legend: {
						display: false
					},
					tooltips: {
						intersect: false
					},
					hover: {
						intersect: true
					},
					plugins: {
						filler: {
							propagate: false
						}
					},
					scales: {
						xAxes: [{
							reverse: true,
							gridLines: {
								color: "rgba(0,0,0,0.05)"
							}
						}],
						yAxes: [{
							ticks: {
								stepSize: 500
							},
							display: true,
							borderDash: [5, 5],
							gridLines: {
								color: "rgba(0,0,0,0)",
								fontColor: "#fff"
							}
						}]
					}
				}
			});
		});
	</script>
	<script>
		$(function() {
			// Pie chart
			new Chart(document.getElementById("chartjs-dashboard-pie"), {
				type: "pie",
				data: {
					labels: ["Direct", "Affiliate", "E-mail", "Other"],
					datasets: [{
						data: [2602, 1253, 541, 1465],
						backgroundColor: [
							window.theme.primary,
							window.theme.warning,
							window.theme.danger,
							"#E8EAED"
						],
						borderColor: "transparent"
					}]
				},
				options: {
					responsive: !window.MSInputMethodContext,
					maintainAspectRatio: false,
					legend: {
						display: false
					}
				}
			});
		});
	</script>
	<script>
		$(function() {
			$("#datatables-dashboard-projects").DataTable({
				pageLength: 6,
				lengthChange: false,
				bFilter: false,
				autoWidth: false
			});
		});
	</script>
</body>

</html>