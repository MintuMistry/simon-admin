﻿

			<main class="content Profile-box">
				<div class="container-fluid p-0">
					<div class="row justify-content-center">
						<div class="col-md-12 col-xl-9">
							<div class="card">
								<div class="subscription-plan" data-toggle="modal" data-target="#subplan-pop"><a href="#"> View Subscription Plan </a></div>
								<div class="upload-profile-box">
									<div class="upload-box posi-relative">
									     <div class="circle circl-new">
									       <img class="profile-pic profile-pic-new" src="<?php echo base_url()."/writable/uploads/".$admin_data->photo ?>" alt="profile images">
									     </div>
									     <div class="p-image p-image-new">
									     	<i class="fas fa-pencil-alt upload-button"></i>
									        <input class="file-upload" type="file" accept="image/*">
									     </div>
									</div>
								</div>
								<div class="card-header mt-4">
									<h5 class="card-title mb-0">Personal info</h5>

								</div>
								<div class="card-body pt-0">
								<form id="formfont" class="form-horizontal" role="form" method="post" action="<?php echo base_url('/admin/Profile_update'); ?>/<?=$admin_data->admin_id ?>" enctype="multipart/form-data">
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="">Full Name</label>
												<input type="text" class="form-control" id="" placeholder="Enter Full Name">
											</div>
											<div class="form-group col-md-6">
												<label for="">Phone Number</label>
												<input type="text" class="form-control" id="" placeholder="Enter Your Phone Number">
											</div>
										</div>
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="">Email</label>
												<input type="email" class="form-control" id="" placeholder="example@gmail.com">
											</div>
											
										</div>

										<h5 class="card-title pl-lg-0 mb-0">Organization Information </h5>
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="inputAddress">Organization  Name</label>
												<input type="text" class="form-control" id="" placeholder="Simonsays service dummy text">
											</div>
											<div class="form-group col-md-6">
												<label for="inputAddress2">Organization  ID</label>
												<input type="text" class="form-control" id="" placeholder="DRK2354R">
											</div>
										</div>
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="inputCity">Organization Service Category</label>
												<input type="text" class="form-control" id="" placeholder="Dummy text">
											</div>
											<div class="form-group col-md-6">
												<label for="inputCity">Organization Mail ID</label>
												<input type="text" class="form-control" id="" placeholder="example@gmail.com">
											</div>
											
										</div>
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="inputCity">Street Address</label>
												<input type="text" class="form-control" id="" placeholder="1234 Main St">
											</div>
											<div class="form-group col-md-6">
												<label for="inputCity">City</label>
												<input type="text" class="form-control" id="" placeholder="Amsterdam">
											</div>
											
										</div>
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="inputCity">State</label>
												<input type="text" class="form-control" id="" placeholder="New York">
											</div>
											<div class="form-group col-md-6">
												<label for="inputCity">Zip</label>
												<input type="text" class="form-control" id="" placeholder="10001">
											</div>
											
										</div>
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="inputZip">Country</label>
												<input type="text" class="form-control" id="" placeholder="United States">
											</div>
											<div class="form-group col-md-6 text-right">
												<a class="btn profile-submit-btn mt-4" href="#"> Submit </a>
											</div>
										</div>										
									</form>
								</div>
							</div>
						</div>
							<!--------------- Add Installer Modal ---------------->
			     			<div class="modal fade show" id="subplan-pop" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
								<div class="modal-dialog subsc-size" role="document">
									<div class="modal-content">
										<div class="modal-header">
										   <h3 class="modal-title"> Set Up Your Subscription Plan </h3>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						                      <span aria-hidden="true">×</span>
						                    </button>
										</div>
										<div class="modal-body subscplan">
												<table class="table table-borderless">
												  <tbody>
												    <tr>
												      <th>Plan</th>
												      <td><a href="subscription_plan.html"> Upgrade Plan </a></td>
												    </tr>
												    <tr>
												      <th>Installer</th>
												      <td>10</td>
												    </tr>
												   <tr>
												      <th>Billing Cycle</th>
												      <td>30days</td>
												    </tr>
												    <tr>
												      <th>Next Billing Date</th>
												      <td>Jan 3, 2020 at 8:06am</td>
												    </tr>
												    <tr>
												      <th>Amount</th>
												      <td>$100 </td>
												    </tr>
												  </tbody>
												</table>
												<div class="alert alert-dismissible fade show d-block cardborder" role="alert">
												  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
												    <span aria-hidden="true"><i class="align-middle mr-2 far fa-fw fa-trash-alt"></i></span>
												  </button>
												  <div class="card-detail-hd">Cradit Card Details</div>
												  <div class="card-number"><span> Cradit Card Number : </span> 67534XXXXXXX </div>
												  <div class="card-exp-date"><span> Exp Date : </span> 03/2023</div>
												</div>	
										</div>
									</div>
								</div>
							</div>
		<!-------- Add Installer End Modal -------->
					</div>

				</div>
			</main>

			<!-- <footer class="footer"></footer> -->
		</div>
	</div>
<!-- <script src="js\settings.js"></script> -->
	<script src="js\app.js"></script>
<script  src="js/upload-img.js"></script>
</body>

</html>