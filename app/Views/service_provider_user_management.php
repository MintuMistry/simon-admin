
			<main class="content dashboard-box">
				<div class="form-row justify-content-md-end pb-3 bordrstyle">
					<div class="form-group col-lg-4">
							<div class="input-group">
								<input type="text" class="form-control" placeholder="Search by Name and Emp ID.">
								<span class="input-group-append">
				                  <button class="btn btn-info" type="button">Go!</button>
				                </span>
							</div>
					</div>
					<div class="form-group col-lg-2">
						<div class="unblock-filter">
						   <select class="custom-select select-height">
							  <option selected="">Block</option>
							  <option value="1">Unblock</option>
							</select>
						</div>
					</div>
					<div class="form-group col-lg-2">
						<div class="btn import_btn w-100" data-toggle="modal" data-target="#add-installer-pop"><i class="align-middle mr-2 fas fa-fw fa-plus"></i> Add User </div>
					</div>

				</div>
		<!--------------- Add Installer Modal ---------------->
			     			<div class="modal fade show" id="add-installer-pop" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
										   <div class="upload-box">
											     <div class="circle">
											       <img class="profile-pic" src="img/dummy-prof.jpg" alt="profile">
											     </div>
											     <div class="p-image pop-update">
											       <i class="fas fa-pencil-alt upload-button"></i>
											        <input class="file-upload" type="file" accept="image/*"/>
											     </div>
											</div>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						                      <span aria-hidden="true">×</span>
						                    </button>
										</div>
										<div class="modal-body">
													<form>
														<div id="formbox">
														  <div class="form-row">
														    <div class="form-group col-md-6">
														      <label for=""> User Name </label>
														      <input type="text" class="form-control" id="" placeholder=" ">
														    </div>
														    <div class="form-group col-md-6">
														      <label for="">User ID Proof </label>
														      <input type="text" class="form-control" id="" placeholder="">
														    </div>
														  </div>
														   <div class="form-row">
														   	<div class="form-group col-md-6">
														      <label for="">Street Address</label>
														      <input type="text" class="form-control" id="" placeholder="">
														    </div>
														    <div class="form-group col-md-6">
														      <label for="">City</label>
														      <input type="text" class="form-control" id="" placeholder="">
														    </div>
														  </div>
														   <div class="form-row">
																<div class="form-group col-md-6">
																	<label for="inputAddress">State</label>
																	<input type="text" class="form-control" id="" placeholder="">
																</div>
																<div class="form-group col-md-6">
																	<label for="inputAddress2">Zip</label>
																	<input type="text" class="form-control" id="" placeholder="">
																</div>
															</div>
															<div class="form-row">
																<div class="form-group col-md-6">
																	<label for="inputCity">Country</label>
																	<input type="text" class="form-control" id="" placeholder="">
																</div>
																<div class="form-group col-md-6">
																	<label for="inputState">User Mobile Number </label>
																	<input type="text" class="form-control" id="" placeholder="">
																</div>
															</div>
															<div class="form-row">
																<div class="form-group col-md-6">
																	<label for="inputZip">Specialty</label>
																	<input type="text" class="form-control" id="" placeholder="">
																</div>
																<div class="form-group col-md-6">
																	<label for="inputZip">Date of Birth</label>
																	<input type="date" data-date="" data-date-format="MMMM DD YYYY" value="" class="form-control" id="" placeholder="">
																</div>
															</div>
															<div class="form-row">
																<div class="form-group col-md-6">
																	<label for="inputZip"> User Email ID </label>
																	<input type="text" class="form-control" id="" placeholder="">
																</div>
																<div class="form-group col-md-6">
																	<label for="inputZip"> User Login Name </label>
																	<input type="text" class="form-control" id="" placeholder="">
																</div>
															</div>
															<div class="form-row">
																<div class="form-group col-md-6">
																	<label for="inputZip">Password </label>
																	<input type="password" class="form-control" id="" placeholder="">
																</div>
															</div>

														  <div class="form-group text-center">
														     <button type="submit" class="btn btn-primary center-block btn-lg addstu_btn"> Submit </button>
														  </div>
													</div>
												</form>
										</div>
										
									</div>
								</div>
							</div>
		<!-------- Add Installer End Modal -------->


		<!--------------- View Installer Modal ---------------->

			     			<div class="modal fade show" id="view-installer-pop" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h3 class="modal-title"> </h3>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						                      <span aria-hidden="true">×</span>
						                    </button>
										</div>
										<div class="modal-body">
											<form>
														<div id="formbox">
														  <div class="form-row">
														    <div class="form-group col-md-6">
														      <label for=""> Employee ID </label>
														      <input type="text" class="form-control" id="" placeholder="DF654" readonly>
														    </div>
														    <div class="form-group col-md-6">
														      <label for="">User Name</label>
														      <input type="text" class="form-control" id="" placeholder="Isabella" readonly>
														    </div>
														  </div>
														   <div class="form-row">
														   	<div class="form-group col-md-6">
														      <label for="">User ID Proof</label>
														      <input type="text" class="form-control" id="" placeholder="Id proof name dummy text" readonly>
														    </div>
														    <div class="form-group col-md-6">
														      <label for="">User Address </label>
														      <input type="text" class="form-control" id="" placeholder="135 Flax Hill Rd #24, Norwalk, CT" readonly>
														    </div>
														  </div>
														   <div class="form-row">
																<div class="form-group col-md-6">
																	<label for="inputAddress">User Mobile Number </label>
																	<input type="text" class="form-control" id="" placeholder="(870) 424-4738" readonly>
																</div>
																<div class="form-group col-md-6">
																	<label for="inputAddress2">Specialty</label>
																	<input type="text" class="form-control" id="" placeholder="5" readonly>
																</div>
															</div>
															<div class="form-row">
																<div class="form-group col-md-6">
																	<label for="inputCity">Date of Birth </label>
																	<input type="text" class="form-control" id="" placeholder="10/22/1990" readonly>
																</div>
																<div class="form-group col-md-6">
																	<label for="inputState">User Email ID </label>
																	<input type="text" class="form-control" id="" placeholder="example@gmail.com" readonly>
																</div>
															</div>
															<div class="form-row">
																<div class="form-group col-md-6">
																	<label for="inputZip">Number of Jobs Done </label>
																	<input type="text" class="form-control" id="" placeholder="57" readonly>
																</div>
																<div class="form-group col-md-6">
																	<label for="inputZip">Number of Jobs Incomplete </label>
																	<input type="text" class="form-control" id="" placeholder="6" readonly>
																</div>
															</div>
															<div class="form-row">
																<div class="form-group col-md-6">
																	<label for="inputZip">Number of Client not Available </label>
																	<input type="text" class="form-control" id="" placeholder="3" readonly>
																</div>
																<div class="form-group col-md-6">
																	<label for="inputZip"> User Login Name </label>
																	<input type="text" class="form-control" id="" placeholder="Bonnie J Smith" readonly>
																</div>
															</div>
															<div class="form-row">
																<div class="form-group col-md-6">
																	<label for="inputZip">Password </label>
																	<input type="password" class="form-control" id="" placeholder="****************" readonly>
																</div>
															</div>

													</div>
													</form>
										</div>
										
									</div>
								</div>
							</div>
				<!-------- View Installer End Modal -------->

				<div class="container-fluid p-0">
					<div class="row">
									<div class="col-12 col-lg-12 mt-minus">
									<div class="table-responsive  border">
									<table class="table mb-0">
										<thead>
											<tr>
												<th scope="col">Emp ID </th>
												<th scope="col" style="">User Name</th>
												<th scope="col" style="">Mobile Number</th>
												<th scope="col" style="">Email ID</th>
												<th scope="col" class="text-center">Number of Jobs <br> Today </th>
												<th scope="col" class="text-center">Number of Jobs <br> Completed Today</th>
												<th scope="col">Action</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<th scope="row">DF654</th>
												<td>Lorem ipsum</td>
												<td> 202 555 0191</td>
												<td>example@gmail.com</td>
												<td class="text-center">17</td>
												<td class="text-center">7</td>
												<td>
													<a href="" data-toggle="modal" data-target="#view-installer-pop"><i class=" align-middle mr-2 fas fa-eye"></i></a>
													<a href=""><i class="align-middle mr-2 far fa-fw fa-trash-alt"></i></a>
													<a href=""><i class="align-middle fas fa-fw fa-ban"></i></a>
												</td>
											</tr>
											<tr>
												<th scope="row">DF654</th>
												<td>Lorem ipsum</td>
												<td> 202 555 0191</td>
												<td>example@gmail.com</td>
												<td class="text-center">17</td>
												<td class="text-center">7</td>
												<td>
													<a href=""><i class=" align-middle mr-2 fas fa-eye"></i></a>
													<a href=""><i class="align-middle mr-2 far fa-fw fa-trash-alt"></i></a>
													<a href=""><i class="align-middle fas fa-fw fa-ban"></i></a>
												</td>
											</tr>
											<tr>
												<th scope="row">DF654</th>
												<td>Lorem ipsum</td>
												<td> 202 555 0191</td>
												<td>example@gmail.com</td>
												<td class="text-center">17</td>
												<td class="text-center">7</td>
												<td>
													<a href=""><i class=" align-middle mr-2 fas fa-eye"></i></a>
													<a href=""><i class="align-middle mr-2 far fa-fw fa-trash-alt"></i></a>
													<a href=""><i class="align-middle fas fa-fw fa-ban"></i></a>
												</td>
											</tr>
											<tr>
												<th scope="row">DF654</th>
												<td>Lorem ipsum</td>
												<td> 202 555 0191</td>
												<td>example@gmail.com</td>
												<td class="text-center">17</td>
												<td class="text-center">7</td>
												<td>
													<a href=""><i class=" align-middle mr-2 fas fa-eye"></i></a>
													<a href=""><i class="align-middle mr-2 far fa-fw fa-trash-alt"></i></a>
													<a href=""><i class="align-middle fas fa-fw fa-ban"></i></a>
												</td>
											</tr>
											<tr>
												<th scope="row">DF654</th>
												<td>Lorem ipsum</td>
												<td> 202 555 0191</td>
												<td>example@gmail.com</td>
												<td class="text-center">17</td>
												<td class="text-center">7</td>
												<td>
													<a href=""><i class=" align-middle mr-2 fas fa-eye"></i></a>
													<a href=""><i class="align-middle mr-2 far fa-fw fa-trash-alt"></i></a>
													<a href=""><i class="align-middle fas fa-fw fa-ban"></i></a>
												</td>
											</tr>

										</tbody>
									</table>


								</div>
						
						</div>
				</div>
			</main>

			<!-- <footer class="footer"></footer> -->
		</div>
	</div>
<!-- <script src="js\settings.js"></script> -->
	<script src="<?php echo base_url(); ?>/public/assets/js/app.js"></script>
	<script  src="<?php echo base_url(); ?>/public/assets/js/upload-img.js"></script>
	
</body>

</html>