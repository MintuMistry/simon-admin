﻿<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="Bootlab">
	<title> Simon Says </title>
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,500;1,600;1,700;1,800;1,900&family=Nunito+Sans:ital,wght@0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="css/upload-img.css">
	<link rel="stylesheet" href="css/classic.css">
</head>

<body>
	<main class="main newbg">
		<div class="container">
			<div class="row align-items-center justify-content-center">
				<div class="col-sm-10 col-md-8 col-lg-10 mt-80">
					<div class="card subscriptionbox">
						<div class="card-body">
							<div class="subsc-billing-btn">Subscription & Billing</div>
							<div class="sub-box">
								<h5>My Subscription Summary</h5>
								<p>Your subscription plan started on Dec 4, 2020 and will automatically renew on Jan 3, 2020 at 8:06am.</p>
							</div>
							<div class="row">
								<div class="col-lg-7">
									<div class="left-sub-box row">
										<div class="form-group  col-lg-10 mb-4">
											<label for="inputCity"><div class="smclr">Enter Number of Service Providers / Users</div></label>
											 <select class="custom-select serviceprovider-dropdown">
												  <option selected="">1</option>
												  <option value="1">2</option>
												  <option value="2">3</option>
												  <option value="3">4</option>
										  	</select>
										</div>
										<div class="col-lg-12 perplandes">
											<p>Plan Per User: <span> $10/Month </span></p>
											<p>Total Amount : <span> $100 </span></p>
											<div class="printbox">
												<table class="table table-borderless print-table">
												  <tbody>
												    <tr>
												      <th><span> Total Monthly Plan Per User : </span> $10/Month</th>
												      <td></td>
												    </tr>
												    <tr>
												      <th><span>Next Invoice  </span> Jan 3, 2020 at 8:06am </th>
												      <td></td>
												    </tr>
												   <tr>
												      <th><span>10x Monthly Auto Renewing Term: </span></th>
												      <td>$10x10=$100</td>
												    </tr>
												    <tr>
												      <th>Total:</th>
												      <td><b> $100 </b></td>
												    </tr>
												  </tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-5">
									<div class="payment-card-box">
										<form>
											<div class="row no-gutters">
												<div class="col-md-12"><div class="paymnt-amo">Payment Amount : $100</div></div>
												<div class="form-group col-md-12">
											       <label>Name on Card</label>
											       <div class="card-holder">
											      		<input type="text" class="form-control" id="" placeholder="">
											  		</div>
											    </div>
											    <div class="form-group col-md-12">
											       <label>Card Number</label>
											       <div class="cord-num">
											      		<input type="Number" class="form-control" id="" placeholder="">
											  		</div>
											    </div>
											    <div class="form-group col-md-6">
											       <label>Expiry Date</label>
											       <div class="exp-date">
											      		<input type="Number" class="form-control" id="" placeholder="MM/YYYY">
											  		</div>
											    </div>
											    <div class="form-group col-md-6">
											       <label>Security Code</label>
											       <div class="sec-code">
											      		<input type="Number" class="form-control" id="" placeholder="">
											      		 <a href="#" class="secqurity-info" data-toggle="tooltip" title="" data-original-title="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor."><i class="fas fa-info-circle"></i></a>
											  		</div>
											    </div>
											    <div class="form-group col-sm-12 text-center mt-2">
												    <div type="submit" class="savepay"><a href="login.html"> Save and Pay </a></div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>

	<script src="<?php echo base_url(); ?>/public/assets/js/app.js"></script>
	<script  src="<?php echo base_url(); ?>/public/assets/js/upload-img.js"></script>
<script>
	$(document).ready(function(){
	  $('[data-toggle="tooltip"]').tooltip();
	});
</script>
</body>

</html>